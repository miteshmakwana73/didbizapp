package com.jenya.didbizdialer.service;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.AlarmClock;
import android.provider.CallLog;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.jenya.didbizdialer.R;
import com.jenya.didbizdialer.adapter.TaskAdapter;
import com.jenya.didbizdialer.jsonurl.Config;
import com.jenya.didbizdialer.util.AndroidMultiPartEntity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static java.util.Calendar.MINUTE;

public class PhoneCallStatesService extends Service implements MediaRecorder.OnInfoListener {
    private static final String TAG = PhoneCallStatesService.class.getSimpleName();

    private Context mContext = PhoneCallStatesService.this;

    private TelephonyManager telephonyManager;
    private PhoneStateListener listener;
    private boolean isOnCall;

    private static boolean isIncoming;

    private static int lastState = TelephonyManager.CALL_STATE_IDLE;
    private static Date callStartTime;
    private static String savedNumber;  //because the passed incoming is only valid in ringing
    private String phNumber;
    private String callDuration;
    private String callType;
    private String callDate;

    private List<String> statuslist;
    private int statusid;

    String HttpUrl = Config.URL_UPDATE_TASK;
    String CallDetailUrl = Config.URL_ADD_CALL_DETAILS;

    ProgressDialog pDialog ;

    Calendar myCalendar = Calendar.getInstance();

    TimePickerDialog mTimePicker;
    DatePickerDialog datePickerDialog;

    int day,month,year,hour,minute;
    int dayFinal,monthFinal,yearFinal,hourFinal,minuteFinal;

    String call;
    int closedialog=0;

    String recordpath,recordnamefile=" ";

    MediaRecorder recorder;
    File audiofile;


    public IBinder onBind(Intent arg0) {

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        showToast("service started");
        isOnCall = false;
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        statuslist = new ArrayList<>();

    }

    protected ReceiverCall stopReceiver = new ReceiverCall() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "received stop broadcast");
            // Stop the service when the notification is tapped
            //unregisterReceiver(stopReceiver);
            // stopSelf();
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Create a new PhoneStateListener
        listener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                if (lastState == state) {
                    //No change, debounce extras
                    return;
                }
                switch (state) {
                    case TelephonyManager.CALL_STATE_IDLE:
                        if (lastState == TelephonyManager.CALL_STATE_RINGING) {
                            //Ring but no pickup-  a miss
                            onMissedCall(getApplicationContext(), savedNumber, callStartTime);
                        } else if (isIncoming) {
                            onIncomingCallEnded(getApplicationContext(), savedNumber, callStartTime, new Date());
                        } else {
                            onOutgoingCallEnded(getApplicationContext(), savedNumber, callStartTime, new Date());
                        }
                        if (isOnCall) {
//                            showToast("Call state: idle");
                            isOnCall = false;
                        }
                        break;
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                       /* File file = new File(Environment.getExternalStorageDirectory()
                                + File.separator + "MIUI/sound_recorder/call_rec");
                        File[] listFile;
                        String[] FilePathStrings;
                        String[] FileNameStrings;

                        if (file.isDirectory()) {
                            listFile = file.listFiles();
                            // Create a String array for FilePathStrings
                            FilePathStrings = new String[listFile.length];
                            // Create a String array for FileNameStrings
                            FileNameStrings = new String[listFile.length];

                            for (int i = 0; i < listFile.length; i++) {
                                // Get the path of the image file
                                FilePathStrings[i] = listFile[i].getAbsolutePath();
                                // Get the name image file
                                FileNameStrings[i] = listFile[i].getName();

                                String name=listFile[i].getName();
                                Toast.makeText(mContext, name, Toast.LENGTH_SHORT).show();
                                String call;
                                SimpleDateFormat sdff = new SimpleDateFormat("yyyyMMddHHmm");
                                call=sdff.format(new Date());
                                String full="Call@"+phNumber+"("+phNumber+")_"+call;

                                String word="helloworld";
                                if(name.contains(full)){
                                    Log.e("Great 'Hello' " , word);
                                    System.err.printf("Great '%s' contains word 'Hello' %n" , word);
                                }else{
                                    Log.e("Sorry 'Hello' " , word);

                                    System.err.printf("Sorry %s does not contains word 'Hello' %n" , word);
                                }

                            }
                        }
*/
                            if (lastState != TelephonyManager.CALL_STATE_RINGING) {
                            isIncoming = false;
                            callStartTime = new Date();
//                            showToast("Outgoing start ");
                            onOutgoingCallStarted(getApplicationContext(), savedNumber, callStartTime);
                        } else {
                            isIncoming = true;
                            callStartTime = new Date();
//                            showToast("Incoming answered ");
                            onIncomingCallAnswered(getApplicationContext(), savedNumber, callStartTime);
                        }
                        isOnCall = true;
                        break;
                    case TelephonyManager.CALL_STATE_RINGING:
                        isIncoming = true;
                        callStartTime = new Date();
                        savedNumber = incomingNumber;
                        onIncomingCallReceived(getApplicationContext(), incomingNumber, callStartTime);
                        showToast("call state: ringing");
                        break;
                }
                lastState = state;
            }
        };

        // Register the listener with the telephony manager
        telephonyManager.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);

        return START_STICKY;
    }

    private void startrecording() {
        String manufacturer = android.os.Build.MANUFACTURER;

        if (!"xiaomi".equalsIgnoreCase(manufacturer)) {
            String out = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss").format(new Date());
            File sampleDir = new File(Environment.getExternalStorageDirectory(), "/Didbiz/recording");
            if (!sampleDir.exists()) {
                sampleDir.mkdirs();
            }
            String file_name = "didbiz";
            try {
                audiofile = File.createTempFile(file_name, ".amr", sampleDir);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String path = Environment.getExternalStorageDirectory().getAbsolutePath();

            recorder = new MediaRecorder();
//                          recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);

            manufacturer = Build.MANUFACTURER;
            if (manufacturer.toLowerCase().contains("xiaomi")) {
                recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            } else {
                recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
            }
//        recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            recorder.setOutputFile(audiofile.getAbsolutePath());



            try {
                recorder.prepare();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            recorder.start();
        }
    }

    void stopRecording() {
        String manufacturer = android.os.Build.MANUFACTURER;
        if (!"xiaomi".equalsIgnoreCase(manufacturer)) {
            recorder.stop();
            recorder.release();
        }
    }

    protected void onIncomingCallReceived(Context ctx, String number, Date start) {
        showToast("received");
        Log.e("received", "received");
    }

    protected void onIncomingCallAnswered(Context ctx, String number, Date start) {
        showToast("receive");
        SimpleDateFormat sdff = new SimpleDateFormat("yyyyMMddHHmm");
        call=sdff.format(new Date());
        Log.e("receive", "receive");
        startrecording();

    }

    protected void onIncomingCallEnded(final Context ctx, String number, Date start, Date end) {
        showToast("incoming call end");
        Log.e("incoming call end", "incoming call end");

        stopRecording();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alert(ctx);
            }
        }, 3000);
    }

    protected void onOutgoingCallStarted(Context ctx, String number, Date start) {
        showToast("outcall call start");
        Log.e("outcall call start", "outcall call start");
        SimpleDateFormat sdff = new SimpleDateFormat("yyyyMMddHHmm");
        call=sdff.format(new Date());
        startrecording();
    }

    protected void onOutgoingCallEnded(final Context ctx, String number, Date start, Date end) {
        showToast("Outgoing call end");
        Log.e("Outgoing call end", "Outgoing call end");
        stopRecording();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alert(ctx);
            }
        }, 3000);    }

    protected void onMissedCall(final Context ctx, String number, Date start) {
        showToast("missed call");
        Log.e("missed call", "missed call");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alert(ctx);
            }
        }, 3000);
    }
    /*private void onIncomingCallAnswered(Context applicationContext, String savedNumber, Date callStartTime) {
    }

    private void onOutgoingCallStarted(Context applicationContext, String savedNumber, Date callStartTime) {
    }

    private void onIncomingCallReceived(Context applicationContext, String incomingNumber, Date callStartTime) {
    }

    private void onOutgoingCallEnded(Context context, String savedNumber, Date callStartTime, Date date) {
    }

    private void onIncomingCallEnded(Context context, String savedNumber, Date callStartTime, Date date) {
    }

    private void onMissedCall(Context context, String savedNumber, Date callStartTime) {
    }*/

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void alert(Context ctx) {
        StringBuffer sb = new StringBuffer();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Cursor cur = getContentResolver().query(CallLog.Calls.CONTENT_URI,
                null, null, null, CallLog.Calls.DATE + " DESC limit 1;");
        //Cursor cur = getContentResolver().query( CallLog.Calls.CONTENT_URI,null, null,null, android.provider.CallLog.Calls.DATE + " DESC");

        int number = cur.getColumnIndex( CallLog.Calls.NUMBER );
        int duration = cur.getColumnIndex( CallLog.Calls.DURATION);
        int type = cur.getColumnIndex(CallLog.Calls.TYPE);
        int date = cur.getColumnIndex(CallLog.Calls.DATE);
        sb.append( "Call Details : \n");
        phNumber = null;
        callDuration = null;
        callType = null;
        callDate = null;
        String dir = null;
        String callDayTime = null;
        while ( cur.moveToNext() ) {
            phNumber = cur.getString( number );
            callDuration = cur.getString( duration );
            callType = cur.getString( type );
            callDate = cur.getString( date );
            callDayTime = new Date(Long.valueOf(callDate)).toString();



            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;

                case CallLog.Calls.REJECTED_TYPE:
                dir="Call disconnectd by user";
                break;

            }

//            sb.append( "\nPhone Number:--- "+phNumber +" \nCall duration in sec :--- "+callDuration );
            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);

            sb.append("\n----------------------------------");
            Log.e("dir",dir);
        }
        cur.close();

        callType=dir;
        callDate=callDayTime;
        Log.e("call ",phNumber+" duration"+callDuration+" type "+callType+" date "+callDate);

        if(!callType.equals("MISSED") && !callDuration.equals("0"))
        {
            String manufacturer = android.os.Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                File file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "MIUI/sound_recorder/call_rec");
        /*File file = new File(Environment.getExternalStorageDirectory()
                + File.separator + "Didbiz/recording");*/
                File[] listFile;
                String[] FilePathStrings;
                String[] FileNameStrings;

                if (file.isDirectory()) {
                    listFile = file.listFiles();
                    // Create a String array for FilePathStrings
                    FilePathStrings = new String[listFile.length];
                    // Create a String array for FileNameStrings
                    FileNameStrings = new String[listFile.length];

                    for (int i = 0; i < listFile.length; i++) {
                        // Get the path of the image file
                        FilePathStrings[i] = listFile[i].getAbsolutePath();
                        // Get the name image file
                        FileNameStrings[i] = listFile[i].getName();

                        String name=listFile[i].getName();
//                Toast.makeText(mContext, name, Toast.LENGTH_SHORT).show();

                        String full="Call@"+phNumber+"("+phNumber+")_"+call;
                        full = full.replaceAll("[+]","00");
                        String numbertemp = phNumber.replaceAll("[+]","00");
                        name = name.replaceAll("[+]","00");

                        Log.e("full",full);

                        if(name.contains(numbertemp))//full
                        {
                            if(name.contains(call))
                            {
                                Log.e("found " , name);

                                recordpath=listFile[i].getAbsolutePath();
//                            recordnamefile=listFile[i].getName();
                                File to = new File(file,full+call+"didbiz.wav");
                                listFile[i].renameTo(to);

                                new UploadFileToServer(to.getAbsolutePath()).execute();
                                recordnamefile=to.getName();

                                break;
                            }
                        }else{
                            Log.e("Not found " , name);
                        }
                    }
                }
            }
            else
            {
            /*File file = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "MIUI/sound_recorder/call_rec");*/
                File file = new File(Environment.getExternalStorageDirectory()
                        + File.separator + "Didbiz/recording");
                File[] listFile;
                String[] FilePathStrings;
                String[] FileNameStrings;

                if (file.isDirectory()) {
                    listFile = file.listFiles();
                    // Create a String array for FilePathStrings
                    FilePathStrings = new String[listFile.length];
                    // Create a String array for FileNameStrings
                    FileNameStrings = new String[listFile.length];

                    for (int i = 0; i < listFile.length; i++) {
                        // Get the path of the image file
                        FilePathStrings[i] = listFile[i].getAbsolutePath();
                        // Get the name image file
                        FileNameStrings[i] = listFile[i].getName();

                        String name=listFile[i].getName();
//                Toast.makeText(mContext, name, Toast.LENGTH_SHORT).show();

                        String full=phNumber;
                        Log.e("full",full);

                        if(name.contains("didbiz"))//didbiz
                        {
                            recordpath=listFile[i].getAbsolutePath();
//                        recordnamefile=listFile[i].getName();

                            //                    new UploadFileToServer(recordpath).execute();
                            File to = new File(file,full+call+"didbiz.wav");

                            listFile[i].renameTo(to);
                            new UploadFileToServer(to.getAbsolutePath()).execute();

                            recordnamefile=to.getName();
                        }

                    }
                }
            }
        }
        else
        {
            recordnamefile=" ";
        }

        startactivity(ctx);
    }

    public void startactivity(final Context ctx) {

        // windowManager.addView(lnrLytMaster,lnrLytVisibleLeftPanelParam);

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ctx, R.style.myDialog);
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

//        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.popupdialognew, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        LinearLayout reminder=(LinearLayout)dialogView.findViewById(R.id.lnreminder);
        LinearLayout savecontact=(LinearLayout)dialogView.findViewById(R.id.lnsavecontact);
        LinearLayout feedback=(LinearLayout)dialogView.findViewById(R.id.lnfeedback);

        TextView name=(TextView)dialogView .findViewById(R.id.tvname);
        TextView tvduration=(TextView)dialogView .findViewById(R.id.tvphone);
        ImageView call=(ImageView) dialogView .findViewById(R.id.imgcall);
        ImageView message=(ImageView) dialogView .findViewById(R.id.imgmessage);
        ImageView callstatus=(ImageView) dialogView .findViewById(R.id.imgcallstatus);
        ImageView imgBackground=(ImageView) dialogView .findViewById(R.id.imgbg);
        Button close=(Button) dialogView .findViewById(R.id.button_close);

        name.setText(""+phNumber);
        int due= Integer.parseInt(callDuration);
        tvduration.setText(String.format("%02d:%02d:%02d",(due/3600), ((due % 3600)/60), (due % 60)));
//        tvduration.setText(callDuration);
        
        int calltp = 0;
        if(callType.equals("OUTGOING"))
        {
//                    tvduration.setText(String.format("%02d:%02d:%02d",(due/3600), ((due % 3600)/60), (due % 60)));
            callstatus.setBackgroundResource(R.drawable.ic_call_made_black_24dp);
            calltp=2;
        }
        else if (callType.equals("INCOMING"))
        {
            calltp=1;
            TaskAdapter.taskid=0;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date=sdf.format(new Date());
            Log.e("date",date);
            TaskAdapter.taskcreated_at="";
//            callstatus.setColorFilter(ContextCompat.getColor(this, R.color.gray), android.graphics.PorterDuff.Mode.MULTIPLY);
            callstatus.setBackgroundResource(R.drawable.ic_call_received_black_24dp);
        }
        else if(callType.equals("MISSED"))
        {
            tvduration.setText("00:00:00");
            calltp=3;
            TaskAdapter.taskid=0;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date=sdf.format(new Date());
            Log.e("date",date);
            TaskAdapter.taskcreated_at="";

//            callstatus.setColorFilter(ContextCompat.getColor(this, R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
            callstatus.setBackgroundResource(R.drawable.ic_call_missed_outgoing_black_24dp);
        }

        sendcalldetails(phNumber,tvduration.getText().toString(),calltp,recordnamefile);
        recordnamefile=" ";

        final AlertDialog alertDialog = dialogBuilder.create();
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(phNumber));
                startActivity(intent);*/

                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:"+phNumber));
                startActivity(callIntent);

                closedialog=0;
                TaskAdapter.taskid=0;
                TaskAdapter.taskcreated_at="";
                alertDialog.hide();

            }
        });

        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"+phNumber));
                intentsms.putExtra("sms_body", "");
                startActivity(intentsms);

                closedialog=0;
                TaskAdapter.taskid=0;
                TaskAdapter.taskcreated_at="";
                alertDialog.hide();
            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closedialog=0;
                TaskAdapter.taskid=0;
                TaskAdapter.taskcreated_at="";
                alertDialog.hide();
                /*if(closedialog==1)
                {
                    closedialog=0;
                    TaskAdapter.taskid=0;
                    TaskAdapter.taskcreated_at="";
                    alertDialog.hide();
                }
                else
                {
                    Toast toast = Toast.makeText(mContext,"Please add the feedback", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }*/
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        lp.windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setAttributes(lp);

        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();


        reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showToast("reminder");

                final Dialog dialog = new Dialog(mContext);
                dialog.setContentView(R.layout.dialog_alarm);
                dialog.setTitle("Reminder");
                dialog.setCanceledOnTouchOutside(true);

                final EditText eddate = (EditText) dialog.findViewById(R.id.date);
                final EditText edtime = (EditText) dialog.findViewById(R.id.time);
                final EditText edmessage = (EditText) dialog.findViewById(R.id.message);
                final Button submit = (Button) dialog.findViewById(R.id.btnsubmit);
                final Button cancel = (Button) dialog.findViewById(R.id.btncancel);

                myCalendar=Calendar.getInstance();
                year=myCalendar.get(Calendar.YEAR);
                month=myCalendar.get(Calendar.MONTH);
                day=myCalendar.get(Calendar.DAY_OF_MONTH);


                final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int yearr, int monthOfYear,
                                          int dayOfMonth) {

                        myCalendar.setTimeInMillis(System.currentTimeMillis() - 1000);

                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        String myFormat = "yyyy/MM/dd"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        eddate.setText(sdf.format(myCalendar.getTime()));

                        yearFinal=yearr;
                        monthFinal=monthOfYear+1;
                        dayFinal=dayOfMonth;

                        myCalendar=Calendar.getInstance();
                        year=myCalendar.get(Calendar.YEAR);
                        month=myCalendar.get(Calendar.MONTH);
                        day=myCalendar.get(Calendar.DAY_OF_MONTH);
                    }

                };

                final TimePickerDialog.OnTimeSetListener time=new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        hourFinal=selectedHour;
                        minuteFinal=selectedMinute;

                        edtime.setText( selectedHour + ":" + selectedMinute);

                    }
                };

                eddate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        datePickerDialog = new DatePickerDialog(mContext, date, year,
                                month, day);
                        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(datePickerDialog.getWindow().getAttributes());
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        lp.gravity = Gravity.CENTER;
                        lp.windowAnimations = R.style.DialogAnimation;
                        datePickerDialog.getWindow().setAttributes(lp);

                        datePickerDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                        datePickerDialog.show();
                    }
                });

                edtime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        myCalendar = Calendar.getInstance();
                        int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
                        int minute = myCalendar.get(MINUTE);

                        mTimePicker=new TimePickerDialog(mContext,time,hour,minute, false);//DateFormat.is24HourFormat(mContext)
                        /*mTimePicker = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                edtime.setText( selectedHour + ":" + selectedMinute);
                                myCalendar.set(Calendar.HOUR_OF_DAY,selectedHour);
                                myCalendar.set(MINUTE,selectedMinute);
                            }
                        }, hour, minute, true);//Yes 24 hour time*/
                        mTimePicker.setTitle("Select Time");
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(mTimePicker.getWindow().getAttributes());
                        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        lp.gravity = Gravity.CENTER;
                        lp.windowAnimations = R.style.DialogAnimation;
                        mTimePicker.getWindow().setAttributes(lp);

                        mTimePicker.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                        mTimePicker.show();

                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final String date = eddate.getText().toString().trim();
                        final String time  = edtime.getText().toString().trim();
                        final String message  = edmessage.getText().toString().trim();

                        if(TextUtils.isEmpty(date) ){
                            eddate.setError("Please enter Title");
                            //Toast.makeText(this,"Please enter email",Toast.LENGTH_LONG).show();
                            return;
                        }

                        if(TextUtils.isEmpty(time)){
                            edtime.setError("Please enter amount");
                            //Toast.makeText(this,"Please enter password",Toast.LENGTH_LONG).show();
                            return;
                        }

                        if(TextUtils.isEmpty(message)){
                            edtime.setError("Please enter message");
                            //Toast.makeText(this,"Please enter password",Toast.LENGTH_LONG).show();
                            return;
                        }

                        Calendar cal = Calendar.getInstance();

                        cal.setTimeInMillis(System.currentTimeMillis());
                        cal.clear();
                        cal.set(yearFinal,monthFinal,dayFinal,hourFinal,minuteFinal);// instead of these lines you can use below lines in comment

//                        cal.set(Calendar.HOUR_OF_DAY, dayFinal);
//                        cal.set(Calendar.MINUTE, minuteFinal);
//                        cal.set(Calendar.SECOND, 0);
//                        cal.set(Calendar.MILLISECOND, 0);

                        Intent i = new Intent(AlarmClock.ACTION_SET_ALARM);
                        i.putExtra(AlarmClock.EXTRA_MESSAGE, message);
                        i.putExtra(AlarmClock.EXTRA_HOUR, hourFinal );
                        i.putExtra(AlarmClock.EXTRA_MINUTES, minuteFinal);
                        i.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
                        mContext.startActivity(i);

                        /*int RQS_1 = 1;

                        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_1, intent, 0);
                        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                        alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);*/

                        /*AlarmManager alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                        Intent intent = new Intent(mContext, AlarmReceiver.class);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);

                        alarmMgr.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);*/

                        dialog.hide();
                    }
                });

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.CENTER;
                lp.windowAnimations = R.style.DialogAnimation;
                dialog.getWindow().setAttributes(lp);

                dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                dialog.show();
            }
        });

        savecontact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closedialog=0;
                TaskAdapter.taskid=0;
                TaskAdapter.taskcreated_at="";
                alertDialog.hide();

                final Intent intent = new Intent(Intent.ACTION_INSERT_OR_EDIT);
                intent.putExtra(ContactsContract.Intents.Insert.PHONE, phNumber);
                intent.setType(Contacts.People.CONTENT_ITEM_TYPE);
                startActivity(intent);
//                Toast.makeText(ctx, "Saved", Toast.LENGTH_SHORT).show();
            }
        });

        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showToast("feedback");

                /*Intent intent = new Intent(PhoneCallStatesService.this,FeedbackActivity.class);
                startActivity(intent);*/

                final Dialog dialog = new Dialog(mContext);
                dialog.setContentView(R.layout.dialog_feedback);
                dialog.setTitle("Feedback");
                dialog.setCanceledOnTouchOutside(false);


                // set the custom dialog components - text, image and button
                final EditText status = (EditText) dialog.findViewById(R.id.status);
                final EditText comment = (EditText) dialog.findViewById(R.id.comment);
                final Button submit = (Button) dialog.findViewById(R.id.btnsubmit);
                final Button cancel = (Button) dialog.findViewById(R.id.btncancel);

                statuslist.clear();
                statuslist.add(0+" "+"pending");
                statuslist.add(1+" "+"Complete");
                statuslist.add(2+" "+"Next day");
                statuslist.add(3+" "+"Cancel");

               /* Drawable img = mContext.getResources().getDrawable(
                        R.drawable.ic_account_box_black_24dp);
                img.setBounds(30, 10, 10, 10);
                status.setCompoundDrawables(img, null, null, null);*/

                status.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        showPopupstatus(view,status);
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final String sstatus = status.getText().toString().trim();
                        final String scomment  = comment.getText().toString().trim();

                        if(TextUtils.isEmpty(sstatus) ){
                            status.setError("Please enter Title");
                            return;
                        }

                        if(TextUtils.isEmpty(scomment)){
                            comment.setError("Please enter amount");
                            return;
                        }

                        if(scomment.length() < 4)
                        {
                            comment.setError("minimum 4 character required");
                            return;
                        }
                        submittaskinfo(dialog,phNumber,scomment);
                    }
                });

                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                lp.gravity = Gravity.CENTER;
                lp.windowAnimations = R.style.DialogAnimation;
                dialog.getWindow().setAttributes(lp);

                dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                dialog.show();
//                dialog.show();

            }
        });

    }

    private void sendcalldetails(final String phNumber, final String callDuration, final int callType, final String recordname) {

        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Loading...!");
        pDialog.setCancelable(false);
//        pDialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(pDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        lp.windowAnimations = R.style.DialogAnimation;
        pDialog.getWindow().setAttributes(lp);

        pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//        pDialog.show();

        final int taskid=TaskAdapter.taskid;

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, CallDetailUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.

//                        pDialog.hide();
                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");

                            Log.e("data",phNumber+"/"+callDuration+"/"+callType);
                            if (status1.equals("true")) {

                                //TaskAdapter.taskid=0;
                                //TaskAdapter.taskcreated_at="";
                                showToast(msg);

                            } else {
                                showToast(msg);
                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        pDialog.hide();

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {

            SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
            String  user_id = sharedPreferences.getString("id","");
//          username = sharedPreferences.getString("username","");
//          email = sharedPreferences.getString("email","");
//          department_id = sharedPreferences.getString("department_id","");
            String api_key = sharedPreferences.getString("api_key","");
            //                password = sharedPreferences.getString("password","");

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                /*String credentials = "username:password";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);*/
                headers.put("x-api-key", api_key);

                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String date=sdf.format(new Date());
                Log.e("date",date);

                /*SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String created_at=sdff.format(new Date());*/

                if(TaskAdapter.taskcreated_at.equals(""))
                {
                    SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    TaskAdapter.taskcreated_at=sdff.format(new Date());
                }

                String tskval;
                if(TaskAdapter.taskid==0)
                {
                    tskval=" ";
                }
                else {
                    tskval = String.valueOf(TaskAdapter.taskid);
                }

                Log.e("data",taskid+"/"+callDuration+"/"+callType);

                // Adding All values to Params.
                params.put("user_id", String.valueOf(user_id));
                params.put("task_id", String.valueOf(tskval));
                params.put("number", String.valueOf(phNumber));
                params.put("type", String.valueOf(callType));
                params.put("duration", callDuration);
                params.put("recording", recordname);
                params.put("date", date);
                params.put("created_at", TaskAdapter.taskcreated_at);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void submittaskinfo(final Dialog dialog, final String phNumber, final String comment) {

        pDialog = new ProgressDialog(mContext);
        pDialog.setMessage("Loading...!");
        pDialog.setCancelable(false);
//        pDialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(pDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        lp.windowAnimations = R.style.DialogAnimation;
        pDialog.getWindow().setAttributes(lp);

        pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//        pDialog.show();

        final int taskid=TaskAdapter.taskid;

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.

                        pDialog.hide();
                        dialog.hide();
                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");

                            Log.e("data",taskid+"/"+statusid+"/"+comment);
                            if (status1.equals("true")) {
                                closedialog=1;
                                TaskAdapter.taskid=0;
                                TaskAdapter.taskcreated_at="";
                                showToast(msg);

                            } else {
                                showToast(msg);

                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        pDialog.hide();

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                /*String credentials = "username:password";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);*/
                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
//                user_id = sharedPreferences.getString("id","");
//                username = sharedPreferences.getString("username","");
//                email = sharedPreferences.getString("email","");
//                department_id = sharedPreferences.getString("department_id","");
//                name = sharedPreferences.getString("name","");
                String api_key = sharedPreferences.getString("api_key","");
//                password = sharedPreferences.getString("password","");
                headers.put("x-api-key", api_key);

                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String date=sdf.format(new Date());
                Log.e("date",date);

               /* SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                TaskAdapter.taskcreated_at=sdff.format(TaskAdapter.taskcreated_at);*/

                if(TaskAdapter.taskcreated_at.equals(""))
                {
                    SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    TaskAdapter.taskcreated_at=sdff.format(new Date());
                }

                String tskval;
                if(TaskAdapter.taskid==0)
                {
                    tskval=" ";
                }
                else {
                    tskval = String.valueOf(TaskAdapter.taskid);
                }

                String stsval;
                if(statusid==0)
                {
                    stsval=" ";
                }
                else {
                    stsval = String.valueOf(statusid);
                }

                Log.e("data",taskid+"/"+statusid+"/"+comment);

                // Adding All values to Params.
                params.put("task_id", String.valueOf(tskval));
                params.put("status", String.valueOf(stsval));
                params.put("comment", comment);
                params.put("contact_no", phNumber);
                params.put("date", date);
                params.put("created_at", TaskAdapter.taskcreated_at);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    public void showPopupstatus(View v, final EditText status) {

        PopupMenu popup = new PopupMenu(this, v);

        int p=1;
        for (String s : statuslist)
        {
            s = s.replaceAll("\\d","").trim();
            Log.i("Member name: ", s);

            popup.getMenu().add(s);
        }
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
//                Log.e("item id ", String.valueOf(menuItem.getItemId()));
                status.setText(menuItem.getTitle());
                String sitename=status.getText().toString();
//                site.replace("^A-Za-z", "").trim();

                for (int i = 0; i<statuslist.size(); i++) {
                    Log.i("Member name: ", statuslist.get(i));
                    String strOrig = statuslist.get(i);
                    Log.e("sdsdrfhglasghdlu", statuslist.get(i));
                    boolean find;


                    if (strOrig.toLowerCase().indexOf(sitename.toLowerCase()) > -1) {
                        find = true;
                        String a = strOrig.substring(0, strOrig.indexOf(' '));

                        statusid=Integer.parseInt(a);
                        Log.e("int index", String.valueOf(statusid));

                    }
                }

                return false;


            }
        });

    }

    @Override
    public void onInfo(MediaRecorder mediaRecorder, int i, int i1) {
        showToast("info listoner");
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        String path;
        long totalSize = 0;
        public UploadFileToServer(String thumbnail) {
            path=thumbnail;
        }

        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            //progressBar.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
           /* pDialog = new ProgressDialog(mContext);
            pDialog.setMessage("Loading...!");
            pDialog.setCancelable(false);
//        pDialog.show();

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(pDialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;
            lp.windowAnimations = R.style.DialogAnimation;
            pDialog.getWindow().setAttributes(lp);

            pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
            pDialog.show();*/
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;


            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Config.FILE_UPLOAD_URL);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                File sourceFile = new File(path);
                //File docFile = new File(docUri.getPath());

                // Adding file data to http body
                entity.addPart("image", new FileBody(sourceFile));

                /*// Extra parameters if you want to pass to server
                entity.addPart("website",
                        new StringBody("www.androidhive.info"));
                entity.addPart("email", new StringBody("abc@gmail.com"));*/

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Response from server: " + result);
            showToast(result.trim());
            // showing the server response in an alert dialog
            //showAlert(result);
//            pDialog.hide();

            super.onPostExecute(result);
        }

    }

    @Override
    public void onDestroy() {
        try{
            if(stopReceiver!=null)
                unregisterReceiver(stopReceiver);
            stopSelf();

        }catch(Exception e)
        {
            Log.e("Service Destroy Error",e.getMessage());
        }
    }

/*
    public void getLastCallDetails(Context context) {

        CallDetails callDetails = new CallDetails();

        Uri contacts = CallLog.Calls.CONTENT_URI;
        try {

            Cursor managedCursor = context.getContentResolver().query(contacts, null, null, null, android.provider.CallLog.Calls.DATE + " DESC limit 1;");

            int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
            int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
            int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
            int incomingtype = managedCursor.getColumnIndex(String.valueOf(CallLog.Calls.INCOMING_TYPE));

            if(managedCursor.moveToFirst()){ // added line

                while (managedCursor.moveToNext()) {
                    String callType;
                    String phNumber = managedCursor.getString(number);
                    String callerName = getContactName(context, phNumber);
                    if(incomingtype == -1){
                        callType = "incoming";
                    }
                    else {
                        callType = "outgoing";
                    }
                    String callDate = managedCursor.getString(date);
                    String callDayTime = new      Date(Long.valueOf(callDate)).toString();

                    String callDuration = managedCursor.getString(duration);

                    callDetails.setCallerName(callerName);
                    callDetails.setCallerNumber(phNumber);
                    callDetails.setCallDuration(callDuration);
                    callDetails.setCallType(callType);
                    callDetails.setCallTimeStamp(callDayTime);

                }
            }
            managedCursor.close();

        } catch (SecurityException e) {
            Log.e("Security Exception", "User denied call log permission");

        }

        return callDetails;

    }
*/
}
