package com.jenya.didbizdialer.service;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.*;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.telephony.*;
import android.util.Log;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jenya.didbizdialer.R;
import com.jenya.didbizdialer.adapter.TaskAdapter;
import com.jenya.didbizdialer.jsonurl.Config;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SmsReciever extends BroadcastReceiver {

    private static final String TAG = "Message recieved";
    static ProgressDialog pDialog;
    static String SMSReceievedUrl = Config.URL_MESSAGE_ADD;


    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle pudsBundle = intent.getExtras();
        Object[] pdus = (Object[]) pudsBundle.get("pdus");
        SmsMessage messages = SmsMessage.createFromPdu((byte[]) pdus[0]);
        Log.i(TAG, messages.getMessageBody());
        showToast(context,"SMS Received : " + messages.getMessageBody());

        SharedPreferences sharedPreferences = context.getSharedPreferences("MyFile", 0);
        String  user_id = sharedPreferences.getString("id","");
        String  myphonenumber = sharedPreferences.getString("phone","");

     /*Log.e("number"+messages.getDisplayMessageBody(),"message "+messages.getDisplayOriginatingAddress());
     Log.e("number"+messages.getEmailBody(),"message "+messages.getEmailFrom());
     Log.e("number"+messages.getPseudoSubject(),"message "+messages.getServiceCenterAddress());
     Log.e("number"+messages.getPseudoSubject(),"message "+messages.getServiceCenterAddress());*/

     String from=messages.getOriginatingAddress();
     String to=messages.getDisplayOriginatingAddress();
     String message=messages.getMessageBody();
     String type="1"; //1=local,2=international
     String created_at=messages.getDisplayOriginatingAddress();
     String in_out="1";//1=incoming,2=outgoing
     sendmessagedetal(context,from,myphonenumber,message,1,1);
 }

    public static void sendmessagedetal(final Context context, final String from, final String to, final String message, final int local_international,final int inout) {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading...!");
        pDialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(pDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        lp.windowAnimations = R.style.DialogAnimation;
        pDialog.getWindow().setAttributes(lp);

        pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//        pDialog.show();

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, SMSReceievedUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {


                        pDialog.hide();
                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status1.equals("true")) {


                            } else {
                            }

                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        pDialog.hide();

                        if( volleyError instanceof NoConnectionError) {
                            showToast(context,"No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast(context,"Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast(context,"Server error!");
                        }
                        else
                        {
                            showToast(context,volleyError.toString());
                        }

                    }
                }) {

            SharedPreferences sharedPreferences = context.getSharedPreferences("MyFile", 0);
            String  user_id = sharedPreferences.getString("id","");
            String  myphonenumber = sharedPreferences.getString("phone","");
            //          username = sharedPreferences.getString("username","");
//          email = sharedPreferences.getString("email","");
//          department_id = sharedPreferences.getString("department_id","");
            String api_key = sharedPreferences.getString("api_key","");
            //                password = sharedPreferences.getString("password","");

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                /*String credentials = "username:password";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);*/
                headers.put("x-api-key", api_key);

                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date=sdf.format(new Date());
                Log.e("date",date);

                // Adding All values to Params.
                params.put("user_id", String.valueOf(user_id));
                params.put("sms_from", String.valueOf(from));
                params.put("sms_to", String.valueOf(to));
                params.put("message", String.valueOf(message));
                params.put("type", String.valueOf(local_international));
                params.put("created_at", date);
                params.put("in_out", String.valueOf(inout));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(context);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    public static void showToast(Context context,String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }
}