package com.jenya.didbizdialer.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.MenuItem;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.jenya.didbizdialer.R;
import com.jenya.didbizdialer.activity.DashboardActivity;
import com.jenya.didbizdialer.activity.DialerActivity;
import com.jenya.didbizdialer.activity.HistoryActivity;
import com.jenya.didbizdialer.activity.MessageActivity;
import com.jenya.didbizdialer.activity.TaskActivity;

/**
 * Created by User on 30/05/2018.
 */

public class BottomNavigationViewHelper {

    private static final String TAG = "BottomNavigationViewHel";
    static Intent i;

    public static void setupBottomNavigationView(BottomNavigationViewEx bottomNavigationViewEx){
        Log.d(TAG, "setupBottomNavigationView: Setting up BottomNavigationView");
        bottomNavigationViewEx.enableAnimation(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);
        bottomNavigationViewEx.enableShiftingMode(false);
        bottomNavigationViewEx.setTextVisibility(false);
//        bottomNavigationViewEx.setIconSizeAt(2,15,48);
//        bottomNavigationViewEx.setIconMarginTop(2, BottomNavigationViewEx.dp2px(co, 4));


    }

    public static void enableNavigation(final Context context, final Activity callingActivity, final BottomNavigationViewEx view){
        view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.i_home:
//                        Toast.makeText(context, "Home", Toast.LENGTH_SHORT).show();
                        i = new Intent(context, DashboardActivity.class);//ACTIVITY_NUM = 0
                        context.startActivity(i);
                        callingActivity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        callingActivity.finish();
                        break;

                    case R.id.i_task:
//                        Toast.makeText(context, "Search", Toast.LENGTH_SHORT).show();

                        i  = new Intent(context, TaskActivity.class);//ACTIVITY_NUM = 1
                        context.startActivity(i);
                        callingActivity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        callingActivity.finish();
                        break;

                    case R.id.i_empty:
                      return false;

                    case R.id.i_message:
//                        Toast.makeText(context, "Category", Toast.LENGTH_SHORT).show();

                        i = new Intent(context, MessageActivity.class);//ACTIVITY_NUM = 3
                        context.startActivity(i);
                        callingActivity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        callingActivity.finish();

                        break;

                    case R.id.i_history:
//                        Toast.makeText(context, "Favourite", Toast.LENGTH_SHORT).show();

                        i = new Intent(context, HistoryActivity.class);//ACTIVITY_NUM = 4
                        context.startActivity(i);
                        callingActivity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        callingActivity.finish();
                        break;

                }


                return false;
            }
        });
    }

}
