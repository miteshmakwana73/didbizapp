package com.jenya.didbizdialer.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.jenya.didbizdialer.R;
import com.jenya.didbizdialer.util.BottomNavigationViewHelper;

public class DialerActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;
    String title;

    private static final int ACTIVITY_NUM = 2;
    private Context mContext=DialerActivity.this;

    EditText edtPhoneNo;
    TextView lblinfo;
    Button delete;
    FloatingActionButton fab;

    //    Button dial, clear, delete, star, hash, one, two, three, four, five, six, seven, eight, nine, zero;
    LinearLayout dial, clear,  star, hash, one, two, three, four, five, six, seven, eight, nine, zero;
    String phoneNo="";
    int count=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialer_new);

        setupBottomNavigationView();

        edtPhoneNo = (EditText) findViewById(R.id.edtPhoneNumber);
        edtPhoneNo.setInputType(android.text.InputType.TYPE_NULL);

        lblinfo = (TextView) findViewById(R.id.lblinfo);
        zero = (LinearLayout) findViewById(R.id.btnZero);

        delete = (Button) findViewById(R.id.btndel);
        fab=(FloatingActionButton)findViewById(R.id.fab);


        delete.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                edtPhoneNo.setText("");
                return false;
            }
        });

        zero.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                count=1;
                return false;
            }
        });

        fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.pink)));

        fab.setRippleColor(getResources().getColor(R.color.colorPrimary));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callprocess(view);
            }
        });

        /*dial = (Button) findViewById(R.id.btnCall);
        clear = (Button) findViewById(R.id.btnClearAll);
        delete = (Button) findViewById(R.id.btndel);
        star = (Button) findViewById(R.id.btnAterisk);
        hash = (Button) findViewById(R.id.btnHash);
        one = (Button) findViewById(R.id.btnOne);
        two = (Button) findViewById(R.id.btnTwo);
        three = (Button) findViewById(R.id.btnThree);
        four = (Button) findViewById(R.id.btnFour);
        five = (Button) findViewById(R.id.btnFive);
        six = (Button) findViewById(R.id.btnSix);
        seven = (Button) findViewById(R.id.btnSeven);
        eight = (Button) findViewById(R.id.btnEight);
        nine = (Button) findViewById(R.id.btnNine);
        zero = (Button) findViewById(R.id.btnZero);

        dial.setOnClickListener(this);
        clear.setOnClickListener(this);
        delete.setOnClickListener(this);
        star.setOnClickListener(this);
        hash.setOnClickListener(this);
        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);
        four.setOnClickListener(this);
        five.setOnClickListener(this);
        six.setOnClickListener(this);
        seven.setOnClickListener(this);
        eight.setOnClickListener(this);
        nine.setOnClickListener(this);
        zero.setOnClickListener(this);*/

    }

    private void callprocess(final View view) {
        if (phoneNo.trim().equals("")) {
            lblinfo.setText("Please enter a number to call on!");
            Toast.makeText(this, "Dial number first", Toast.LENGTH_SHORT).show();
        } else {
            lblinfo.setVisibility(View.GONE);
            Boolean isHash = false;
            if (phoneNo.subSequence(phoneNo.length() - 1, phoneNo.length()).equals("#")) {
                phoneNo = phoneNo.substring(0, phoneNo.length() - 1);
                String callInfo = "tel:" + phoneNo + Uri.encode("#");
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse(callInfo));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(callIntent);
            } else {
                String callInfo = "tel:" + phoneNo;
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse(callInfo));
                startActivity(callIntent);
            }
        }
        view.setClickable(false);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setClickable(true);
            }
        }, 3000);
    }

    public void buttonClickEvent(final View view) {
        phoneNo = edtPhoneNo.getText().toString();
        lblinfo.setVisibility(View.GONE);
        try {

            switch (view.getId()) {
                case R.id.btnAterisk:
                    lblinfo.setText("");
                    phoneNo += "*";
                    edtPhoneNo.setText(phoneNo);
                    break;
                case R.id.btnHash:
                    lblinfo.setText("");
                    phoneNo += "#";
                    edtPhoneNo.setText(phoneNo);
                    break;
                case R.id.btnZero:
                    if(count==0)
                    {
                        lblinfo.setText("");
                        phoneNo += "0";
                        edtPhoneNo.setText(phoneNo);
                    }
                    else
                    {
                        lblinfo.setText("");
                        phoneNo += "+";
                        edtPhoneNo.setText(phoneNo);
                        count=0;
                    }

                    break;
                case R.id.btnOne:
                    lblinfo.setText("");
                    phoneNo += "1";
                    edtPhoneNo.setText(phoneNo);
                    break;
                case R.id.btnTwo:
                    lblinfo.setText("");
                    phoneNo += "2";
                    edtPhoneNo.setText(phoneNo);
                    break;
                case R.id.btnThree:
                    lblinfo.setText("");
                    phoneNo += "3";
                    edtPhoneNo.setText(phoneNo);
                    break;
                case R.id.btnFour:
                    lblinfo.setText("");
                    phoneNo += "4";
                    edtPhoneNo.setText(phoneNo);
                    break;
                case R.id.btnFive:
                    lblinfo.setText("");
                    phoneNo += "5";
                    edtPhoneNo.setText(phoneNo);
                    break;
                case R.id.btnSix:
                    lblinfo.setText("");
                    phoneNo += "6";
                    edtPhoneNo.setText(phoneNo);
                    break;
                case R.id.btnSeven:
                    lblinfo.setText("");
                    phoneNo += "7";
                    edtPhoneNo.setText(phoneNo);
                    break;
                case R.id.btnEight:
                    lblinfo.setText("");
                    phoneNo += "8";
                    edtPhoneNo.setText(phoneNo);
                    break;
                case R.id.btnNine:
                    lblinfo.setText("");
                    phoneNo += "9";
                    edtPhoneNo.setText(phoneNo);
                    break;
                case R.id.btndel:
                    lblinfo.setText("");
                    if (phoneNo != null && phoneNo.length() > 0) {
                        phoneNo = phoneNo.substring(0, phoneNo.length() - 1);
                    }

                    edtPhoneNo.setText(phoneNo);
                    break;
                /*case R.id.btnClearAll:
                    lblinfo.setText("");
                    edtPhoneNo.setText("");
                    break;*/
                /*case R.id.btnCall:
                    if (phoneNo.trim().equals("")) {
                        *//*lblinfo.setVisibility(View.VISIBLE);
                        lblinfo.setText("Please enter a number to call on!");*//*
                        Toast.makeText(this, "Dial number first", Toast.LENGTH_SHORT).show();
                    } else {
                        lblinfo.setVisibility(View.GONE);
                        Boolean isHash = false;
                        if (phoneNo.subSequence(phoneNo.length() - 1, phoneNo.length()).equals("#")) {
                            phoneNo = phoneNo.substring(0, phoneNo.length() - 1);
                            String callInfo = "tel:" + phoneNo + Uri.encode("#");
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse(callInfo));
                            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            startActivity(callIntent);
                        } else {
                            String callInfo = "tel:" + phoneNo;
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse(callInfo));
                            startActivity(callIntent);
                        }
                    }
                    view.setEnabled(false);

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            view.setEnabled(true);
                        }
                    }, 3000);
                    break;*/
            }

        } catch (Exception ex) {

        }
    }

    private void setupBottomNavigationView(){
        Log.e("bottom", "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bnve);

        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        BottomNavigationViewHelper.enableNavigation(mContext,this,bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);

    }


    /*@Override
    public void onBackPressed() {

        *//*android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(DialerActivity.this,R.style.MyDialogTheme);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(true)
                .setPositiveButton(R.string.YES, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id2) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        System.exit(0);
                        // MainActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton(R.string.NO, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id2) {
                    }
                });
        // Create the AlertDialog object and return it
        builder.show();*//*

    }*/
}
