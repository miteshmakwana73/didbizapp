package com.jenya.didbizdialer.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.jenya.didbizdialer.R;
import com.jenya.didbizdialer.fragment.CompletedTaskFragment;
import com.jenya.didbizdialer.fragment.PandingTaskFragment;
import com.jenya.didbizdialer.util.BottomNavigationViewHelper;
import com.savvyapps.togglebuttonlayout.Toggle;
import com.savvyapps.togglebuttonlayout.ToggleButtonLayout;

import kotlin.Unit;
import kotlin.jvm.functions.Function2;

public class TaskActivity extends AppCompatActivity
        implements PandingTaskFragment.OnFragmentInteractionListener, CompletedTaskFragment.OnFragmentInteractionListener {
    private static final int ACTIVITY_NUM = 0;
    private Context mContext=TaskActivity.this;
    FloatingActionButton fab;
    ToggleButtonLayout tab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        setupBottomNavigationView();

        getSupportActionBar().setElevation(0);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, PandingTaskFragment.newInstance());
        fragmentTransaction.commit();

        tab=(ToggleButtonLayout)findViewById(R.id.toggletask);
        fab=(FloatingActionButton)findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i  = new Intent(mContext, DialerActivity.class);//ACTIVITY_NUM = 1
                startActivity(i);
                TaskActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });

//        int position = tab.getCheckedTogglePosition();

        tab.setToggled(R.id.toggle_pending,true);

        tab.setOnToggledListener(new Function2<Toggle, Boolean, Unit>() {
            @Override
            public Unit invoke(Toggle toggle, Boolean aBoolean) {
                Fragment fragment = null;

                switch (toggle.getId()) {

                    case R.id.toggle_pending:
//                        showToast("Panding");
                        fragment = PandingTaskFragment.newInstance();
                        break;

                    case R.id.toggle_completed:
//                        showToast("Completed");
                        fragment = CompletedTaskFragment.newInstance();
                        break;

                }

                if (fragment != null) {
                    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frameLayout, fragment);
                    fragmentTransaction.commit();
                }
                return null;
            }
        });


    }

    private void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    private void setupBottomNavigationView(){
        Log.e("bottom", "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bnve);

        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        BottomNavigationViewHelper.enableNavigation(mContext,this,bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);

    }

}
