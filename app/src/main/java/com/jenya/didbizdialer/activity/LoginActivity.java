package com.jenya.didbizdialer.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jenya.didbizdialer.R;
import com.jenya.didbizdialer.jsonurl.Config;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private EditText inputEmail, inputPassword;
        TextView forgoat,title;
    private ProgressBar progressBar;
    Button login,reg;

    String email;
    String password;

    String URL_LOGIN = Config.LOGIN_URL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        CheckLogin();

        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        forgoat = (TextView) findViewById(R.id.tvforgot);
        title = (TextView) findViewById(R.id.login_title);
        login=(Button)findViewById(R.id.btnlogin);

        Typeface tf = Typeface.createFromAsset(this.getAssets(), "fonts/HKNova-Medium.ttf");
        inputEmail.setTypeface(tf);
        inputPassword.setTypeface(tf);
        login.setTypeface(tf);
        forgoat.setTypeface(tf);
        title.setTypeface(tf);



        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = inputEmail.getText().toString().trim();
                password = inputPassword.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    inputEmail.setError("Enter username!");
//                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    inputPassword.setError("Enter password!");
//                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                checkconnection();
            }
        });
    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) LoginActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable

            logindata();

        }else {
            //no connection
            Toast.makeText(LoginActivity.this, "No Connection Found", Toast.LENGTH_SHORT).show();
        }
    }

    private void logindata() {

        progressBar.setVisibility(View.VISIBLE);

        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("success");

                            String msg = jobj.getString("message");

                            if (status.equals("true")) {

//                                addAutoStartup();

                                //move to next page

                                JSONObject obj = new JSONObject(ServerResponse);

                                JSONArray heroarray = obj.getJSONArray("user_data");

                                //Log.e("Data",response);
                                String u_id = null,username = null,email = null,primary_contact = null,department_id = null,department_name = null,f_name = null,l_name = null,name = null,api_key = null;
                                //we have the array named hero inside the object
                                //so here we are getting that json array
                                for(int i=0; i < heroarray.length(); i++) {
                                    JSONObject jsonobject = heroarray.getJSONObject(i);
                                    u_id=jsonobject.getString("id");
                                    username=jsonobject.getString("username");
                                    email=jsonobject.getString("email");
                                    primary_contact=jsonobject.getString("phone");
                                    department_id=jsonobject.getString("department_id");
                                    department_name=jsonobject.getString("department_name");
                                    f_name=jsonobject.getString("first_name");
                                    l_name=jsonobject.getString("last_name");
                                    api_key=jsonobject.getString("api_key");
                                }
//                                JSONArray heroArray = obj.getJSONArray("user_data");

//                                JSONObject heroArray = obj.getJSONObject("user_data");
/*
                                String u_id=heroArray.getString("user_id");
                                String username=heroArray.getString("username");
                                String email=heroArray.getString("email");
                                String department_id=heroArray.getString("department_id");
                                String name=heroArray.getString("name");
                                String api_key=heroArray.getString("api_key");*/


                                SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("id",u_id);
                                editor.putString("username",username);
                                editor.putString("email",email);
                                editor.putString("phone",primary_contact);
                                editor.putString("department_id",department_id);
                                editor.putString("department_name",department_name);
                                editor.putString("f_name",f_name);
                                editor.putString("l_name",l_name);
                                editor.putString("api_key",api_key);
                                editor.putString("password",password);
                                editor.commit();

                                Log.e("email "+email+"pass "+password ,"id "+u_id+" type"+department_id);

                                Intent intent = new Intent(LoginActivity.this, TaskActivity.class);
                                startActivity(intent);
                                finish();

                                Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();

                            } else if (status.equals("2")) {

                                showLocationDialog(msg);

                            }else {
                                Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();

                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            Toast.makeText(LoginActivity.this, "No connection available", Toast.LENGTH_SHORT).show();
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeout error message
                            Toast.makeText(LoginActivity.this, "Oops. Timeout error!", Toast.LENGTH_LONG).show();
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            Toast.makeText(LoginActivity.this, "Server error!", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            Toast.makeText(LoginActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("x-api-key", Config.DEFAULT_LOGIN_KEY);

                /*String credentials = "username:password";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);*/
                /*headers.put("user_id", String.valueOf(1));
                headers.put("department_id", String.valueOf(1));*/

                return headers;
            }
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                //params.put("name", name);
                params.put("username", email);
                params.put("password", password);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void showLocationDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(LoginActivity.this, android.R.style.Theme_Material_Light_Dialog);
        } else {
            builder = new AlertDialog.Builder(LoginActivity.this);
        }
        builder.setTitle(getString(R.string.dialog_title));
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setMessage(msg);

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                    }
                });

        /*String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                    }
                });*/

        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }
    private void CheckLogin() {
        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        String uname = sharedPreferences.getString("username","");
        if (uname.equalsIgnoreCase("")) {

        }
        else
        {
            Intent intent = new Intent(LoginActivity.this, TaskActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void addAutoStartup() {

        try {
            Intent intent = new Intent();
            String manufacturer = Build.MANUFACTURER;
            if ("xiaomi".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
            } else if ("oppo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
            } else if ("vivo".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
            } else if ("Letv".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));
            } else if ("Honor".equalsIgnoreCase(manufacturer)) {
                intent.setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
            }

            List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if  (list.size() > 0) {
                startActivity(intent);
            }
        } catch (Exception e) {
            Log.e("exc" , String.valueOf(e));
        }
    }
}
