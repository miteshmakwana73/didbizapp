package com.jenya.didbizdialer.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.jenya.didbizdialer.R;
import com.jenya.didbizdialer.adapter.CallLogAdapter;
import com.jenya.didbizdialer.jsonurl.Config;
import com.jenya.didbizdialer.model.calllog;
import com.jenya.didbizdialer.util.BottomNavigationViewHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HistoryActivity extends AppCompatActivity {
    private static final int ACTIVITY_NUM = 4;
    private Context mContext=HistoryActivity.this;
    FloatingActionButton fab;

    private RecyclerView recyclerView;
    private List<calllog> taskList;
    private CallLogAdapter adapter;

    ProgressBar progressBar;
    SwipeRefreshLayout sw_refresh;

    TextView status;

    String HttpUrl = Config.URL_CALL_HISTORY;
    String user_id,username,email,department_id,f_name,l_name,name,api_key,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        setupBottomNavigationView();

        SharedPreferences sharedPreferences = getSharedPreferences("MyFile", 0);
        user_id = sharedPreferences.getString("id","");
        username = sharedPreferences.getString("username","");
        email = sharedPreferences.getString("email","");
        department_id = sharedPreferences.getString("department_id","");
        f_name = sharedPreferences.getString("first_name","");
        l_name = sharedPreferences.getString("last_name","");
        api_key = sharedPreferences.getString("api_key","");
        password = sharedPreferences.getString("password","");

        fab=(FloatingActionButton)findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i  = new Intent(mContext, DialerActivity.class);//ACTIVITY_NUM = 1
                startActivity(i);
                HistoryActivity.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        status=(TextView)findViewById(R.id.tvstatus);
        sw_refresh = (SwipeRefreshLayout) findViewById(R.id.sw_refresh);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/HKNova-Medium.ttf");
//        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/proximanovaregular.ttf");
        status.setTypeface(tf);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
                        taskList.clear();


                        checkconnection();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });
        taskList = new ArrayList<>();

        adapter = new CallLogAdapter(mContext,taskList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        checkconnection();
    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable

            status.setVisibility(View.GONE);

            getAllTask();

        }else {
            //no connection
            status.setVisibility(View.VISIBLE);

            showToast("No Connection Found");
        }
    }

    private void getAllTask() {
        progressBar.setVisibility(View.VISIBLE);

        // Creating string request with post method.
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

//                            String msg = jobj.getString("message");

                            if (status1.equals("true")) {

                                //move to next page
//                                Toast.makeText(PerformarEventActivity.this, msg, Toast.LENGTH_SHORT).show();

//                                loadPerformarevent();


                                JSONArray heroArray = jobj.getJSONArray("call_history");

                                //now looping through all the elements of the json array
                                for (int i = 0; i < heroArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject taskObject = heroArray.getJSONObject(i);

                                    //creating a hero object and giving them the values from json object

                                    calllog hero = new calllog(
                                            taskObject.getInt("id"),
                                            taskObject.getString("number"),
                                            taskObject.getString("type"),
                                            taskObject.getString("duration"),
                                            taskObject.getString("task_id"),
                                            taskObject.getString("user_id"),
                                            taskObject.getString("recording"),
                                            taskObject.getString("created_at"),
                                            taskObject.getString("updated_at"));

                                    //adding the hero to herolist
                                    taskList.add(hero);
                                }
                                //Log.e("list", String.valueOf(albumList));
                                //creating custom adapter object
                                adapter = new CallLogAdapter(mContext,taskList);

                                //adding the adapter to listview
                                recyclerView.setAdapter(adapter);


                            } else {
//                                status.setText(msg);
                                status.setVisibility(View.VISIBLE);

//                                showToast(msg);

                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
//                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));
                            getsingletask();

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                /*String credentials = "username:password";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);*/
                headers.put("x-api-key", api_key);

                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("user_id", String.valueOf(user_id));
                params.put("department_id", String.valueOf(department_id));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void getsingletask() {
        progressBar.setVisibility(View.VISIBLE);

        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("success");

//                            String msg = jobj.getString("message");

                            if (status.equals("true")) {

                                //move to next page
//                                Toast.makeText(PerformarEventActivity.this, msg, Toast.LENGTH_SHORT).show();

//                                loadPerformarevent();


                                JSONObject heroArray = jobj.getJSONObject("call_history");

                                calllog hero = new calllog(
                                        heroArray.getInt("id"),
                                        heroArray.getString("number"),
                                        heroArray.getString("type"),
                                        heroArray.getString("duration"),
                                        heroArray.getString("task_id"),
                                        heroArray.getString("user_id"),
                                        heroArray.getString("recording"),
                                        heroArray.getString("created_at"),
                                        heroArray.getString("updated_at"));

                                taskList.add(hero);

                                //Log.e("list", String.valueOf(albumList));
                                //creating custom adapter object
                                adapter = new CallLogAdapter(mContext,taskList);

                                //adding the adapter to listview
                                recyclerView.setAdapter(adapter);


                            } else {
//                                showToast(msg);
                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
//                            Log.e("success",msg);
                        }catch (Exception e) {
                            status.setText("NO DATA");
                            status.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                /*String credentials = "username:password";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);*/
                headers.put("x-api-key", api_key);//1234
                /*headers.put("user_id", String.valueOf(1));
                headers.put("department_id", String.valueOf(1));*/

                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();


                // Adding All values to Params.
                params.put("user_id", String.valueOf(user_id));
                params.put("department_id", String.valueOf(department_id));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                // ProjectsActivity is my 'home' activity
                finish();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }*/


    private void setupBottomNavigationView(){
        Log.e("bottom", "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bnve);

        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        BottomNavigationViewHelper.enableNavigation(mContext,this,bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);

    }


    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    public int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


    public void showToast(String msg)
    {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }
}
