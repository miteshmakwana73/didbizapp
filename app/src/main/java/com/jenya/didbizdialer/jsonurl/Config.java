package com.jenya.didbizdialer.jsonurl;

public class Config {
	//192.168.1.144:9110 local
	//202.131.97.68:8004 server

//	static String SERVER_URL="http://192.168.1.3:8010/";//http://192.168.1.207:81/dialer/";
	static String SERVER_URL="http://rayvatapps.com:8010/";//http://192.168.1.207:81/dialer/";

	public static final String URL_TASK = SERVER_URL+"my-tasks";//;http://192.168.1.3:8008/my-tasks";
	public static final String URL_UPDATE_TASK = SERVER_URL+"update-task";//http://192.168.1.3:8008/update-task";

	public static final String URL_DIDBIZ_CONTACT = SERVER_URL+"app-users";//http://192.168.1.3:8008/app-users";

	public static final String URL_ADD_CALL_DETAILS = SERVER_URL+"add-call-history";
	public static final String URL_CALL_HISTORY = SERVER_URL+"call-history";

	public static final String URL_MESSAGE_ADD = SERVER_URL+"add-sms";
	public static final String URL_MESSAGE_RETRIEVE = SERVER_URL+"get-sms";

    public static final String LOGIN_URL= SERVER_URL+"login";//http://192.168.1.3:8008/login";
    public static final String DEFAULT_LOGIN_KEY = "pATE6yFrYxEWJEZrKG6MIUYPbxf0FdD8";
    public static final String FILE_UPLOAD_URL = SERVER_URL+"uploads.php";
}