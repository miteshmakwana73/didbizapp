package com.jenya.didbizdialer.adapter;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.Image;
import android.net.Uri;
import android.provider.AlarmClock;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.jenya.didbizdialer.R;
import com.jenya.didbizdialer.model.task;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static java.util.Calendar.MINUTE;

/**
 * Created by Mitesh Makwana on 02/05/18.
 */
public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.MyViewHolder> {

    public static int taskid=0;
    public static String taskcomment="";
    public static String taskcontact_no="";
    public static String taskdate="";
    public static String taskcreated_at="";
    private Context mContext;
    private List<task> taskList;

    Typeface tf;

    Calendar myCalendar = Calendar.getInstance();

    TimePickerDialog mTimePicker;
    DatePickerDialog datePickerDialog;

    int day,month,year,hour,minute;
    int dayFinal,monthFinal,yearFinal,hourFinal,minuteFinal;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,number;
        ImageView imginfo;
        RelativeLayout main;
        LinearLayout viewline;

        public MyViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.tvContactName);
//            event = (TextView) view.findViewById(R.id.txteventtitle);
            number = (TextView) view.findViewById(R.id.tvPhoneNumber);
            imginfo = (ImageView) view.findViewById(R.id.imginfo);

//            cdview = (CardView) view.findViewById(R.id.card_view);
            main = (RelativeLayout) view.findViewById(R.id.main);
            viewline = (LinearLayout) view.findViewById(R.id.view);

        }

    }

    public TaskAdapter(Context mContext, List<task> taskList) {
        this.mContext = mContext;
        this.taskList = taskList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_task, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.name.setTypeface(tf);
        holder.number.setTypeface(tf);

        final task task = taskList.get(position);
        Log.e("name",task.getContact_name());

        holder.name.setText(task.getContact_name());

        holder.number.setText(task.getContact_no());

        holder.main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                taskid=task.getId();
                taskcreated_at=task.getCreated_at();
                if(taskcreated_at.equals(""))
                {
                    SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    TaskAdapter.taskcreated_at=sdff.format(new Date());
                }
                else
                {
                    taskcreated_at=task.getCreated_at();
                }
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:"+holder.number.getText().toString()));
                mContext.startActivity(callIntent);
            }
        });

        holder.main.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showPopupstatus(view,task);
                return false;
            }
        });
        if(position==taskList.size()-1)
        {
            holder.viewline.setVisibility(View.GONE);
        }
        else
        {
            holder.viewline.setVisibility(View.VISIBLE);
        }

        holder.imginfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showtaskdetaildialog(view,task);
            }
        });

    }

    private void showtaskdetaildialog(View view, task task) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext, R.style.myDialog);
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View dialogView = inflater.inflate(R.layout.popupdialogtask, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        /*dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });*/
        TextView name=(TextView)dialogView .findViewById(R.id.tvname);
        TextView tvname=(TextView)dialogView .findViewById(R.id.tvcontactname);
        TextView number=(TextView)dialogView .findViewById(R.id.tvnumber);
        TextView tvnumber=(TextView)dialogView .findViewById(R.id.tvcontactnumber);
        TextView detail=(TextView)dialogView .findViewById(R.id.tvdetails);
        TextView tvdetail=(TextView)dialogView .findViewById(R.id.tvtaskdetails);
        TextView datetime=(TextView)dialogView .findViewById(R.id.tvdate);
        TextView tvdatetime=(TextView)dialogView .findViewById(R.id.tvtaskdate);

        tvname.setText(task.getContact_name());
        tvnumber.setText(task.getContact_no());
        tvdetail.setText(task.getComment());
        tvdatetime.setText(task.getCreated_at());

        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");

        name.setTypeface(tf);
//        tvname.setTypeface(tf);
        number.setTypeface(tf);
//        tvnumber.setTypeface(tf);
        detail.setTypeface(tf);
//        tvdetail.setTypeface(tf);
        datetime.setTypeface(tf);
//        tvdatetime.setTypeface(tf);

        final AlertDialog alertDialog = dialogBuilder.create();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        lp.windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setAttributes(lp);

        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alertDialog.show();
    }

    public void showPopupstatus(View v, final task task) {

        PopupMenu popup = new PopupMenu(mContext, v);
        int p=1;
        popup.getMenu().add("Call");
        popup.getMenu().add("Set Reminder");
        popup.getMenu().add("Add Contact");

        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if(menuItem.getTitle()=="Set Reminder")
                {
                    Toast.makeText(mContext, "...", Toast.LENGTH_SHORT).show();
                    final Dialog dialog = new Dialog(mContext);
                    dialog.setContentView(R.layout.dialog_alarm);
                    dialog.setTitle("Reminder");
                    dialog.setCanceledOnTouchOutside(true);

                    final TextView tvtitle = (TextView) dialog.findViewById(R.id.tvtitle);
                    final EditText eddate = (EditText) dialog.findViewById(R.id.date);
                    final EditText edtime = (EditText) dialog.findViewById(R.id.time);
                    final EditText edmessage = (EditText) dialog.findViewById(R.id.message);
                    final Button submit = (Button) dialog.findViewById(R.id.btnsubmit);
                    final Button cancel = (Button) dialog.findViewById(R.id.btncancel);

                    tvtitle.setVisibility(View.VISIBLE);

                    myCalendar= Calendar.getInstance();
                    year=myCalendar.get(Calendar.YEAR);
                    month=myCalendar.get(Calendar.MONTH);
                    day=myCalendar.get(Calendar.DAY_OF_MONTH);


                    final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int yearr, int monthOfYear,
                                              int dayOfMonth) {

                            myCalendar.setTimeInMillis(System.currentTimeMillis() - 1000);

                            // TODO Auto-generated method stub
                            myCalendar.set(Calendar.YEAR, year);
                            myCalendar.set(Calendar.MONTH, monthOfYear);
                            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                            String myFormat = "yyyy/MM/dd"; //In which you need put here
                            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                            eddate.setText(sdf.format(myCalendar.getTime()));

                            yearFinal=yearr;
                            monthFinal=monthOfYear+1;
                            dayFinal=dayOfMonth;

                            myCalendar=Calendar.getInstance();
                            year=myCalendar.get(Calendar.YEAR);
                            month=myCalendar.get(Calendar.MONTH);
                            day=myCalendar.get(Calendar.DAY_OF_MONTH);
                        }

                    };

                    final TimePickerDialog.OnTimeSetListener time=new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                            hourFinal=selectedHour;
                            minuteFinal=selectedMinute;

                            edtime.setText( selectedHour + ":" + selectedMinute);

                        }
                    };

                    eddate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            datePickerDialog = new DatePickerDialog(mContext, date, year,
                                    month, day);
                            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            lp.copyFrom(datePickerDialog.getWindow().getAttributes());
                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            lp.gravity = Gravity.CENTER;
                            lp.windowAnimations = R.style.DialogAnimation;
                            datePickerDialog.getWindow().setAttributes(lp);

                            datePickerDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                            datePickerDialog.show();
                        }
                    });

                    edtime.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            myCalendar = Calendar.getInstance();
                            int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
                            int minute = myCalendar.get(MINUTE);

                            mTimePicker=new TimePickerDialog(mContext,time,hour,minute, false);//DateFormat.is24HourFormat(mContext)
                        /*mTimePicker = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                                edtime.setText( selectedHour + ":" + selectedMinute);
                                myCalendar.set(Calendar.HOUR_OF_DAY,selectedHour);
                                myCalendar.set(MINUTE,selectedMinute);
                            }
                        }, hour, minute, true);//Yes 24 hour time*/
                            mTimePicker.setTitle("Select Time");
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            lp.copyFrom(mTimePicker.getWindow().getAttributes());
                            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            lp.gravity = Gravity.CENTER;
                            lp.windowAnimations = R.style.DialogAnimation;
                            mTimePicker.getWindow().setAttributes(lp);

                            mTimePicker.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                            mTimePicker.show();

                        }
                    });

                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final String date = eddate.getText().toString().trim();
                            final String time  = edtime.getText().toString().trim();
                            final String message  = edmessage.getText().toString().trim();

                            if(TextUtils.isEmpty(date) ){
                                eddate.setError("Please enter Title");
                                //Toast.makeText(this,"Please enter email",Toast.LENGTH_LONG).show();
                                return;
                            }

                            if(TextUtils.isEmpty(time)){
                                edtime.setError("Please enter amount");
                                //Toast.makeText(this,"Please enter password",Toast.LENGTH_LONG).show();
                                return;
                            }

                            if(TextUtils.isEmpty(message)){
                                edtime.setError("Please enter message");
                                //Toast.makeText(this,"Please enter password",Toast.LENGTH_LONG).show();
                                return;
                            }

                            Calendar cal = Calendar.getInstance();

                            cal.setTimeInMillis(System.currentTimeMillis());
                            cal.clear();
                            cal.set(yearFinal,monthFinal,dayFinal,hourFinal,minuteFinal);// instead of these lines you can use below lines in comment

//                        cal.set(Calendar.HOUR_OF_DAY, dayFinal);
//                        cal.set(Calendar.MINUTE, minuteFinal);
//                        cal.set(Calendar.SECOND, 0);
//                        cal.set(Calendar.MILLISECOND, 0);

                            Intent i = new Intent(AlarmClock.ACTION_SET_ALARM);
                            i.putExtra(AlarmClock.EXTRA_MESSAGE, message);
                            i.putExtra(AlarmClock.EXTRA_HOUR, hourFinal );
                            i.putExtra(AlarmClock.EXTRA_MINUTES, minuteFinal);
                            i.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
                            mContext.startActivity(i);

                        /*int RQS_1 = 1;

                        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_1, intent, 0);
                        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                        alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);*/

                        /*AlarmManager alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                        Intent intent = new Intent(mContext, AlarmReceiver.class);
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);

                        alarmMgr.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);*/

                            dialog.hide();
                        }
                    });

                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    lp.copyFrom(dialog.getWindow().getAttributes());
                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    lp.gravity = Gravity.CENTER;
                    lp.windowAnimations = R.style.DialogAnimation;
                    dialog.getWindow().setAttributes(lp);

                    dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                    dialog.show();
                }
                else if (menuItem.getTitle()=="Add Contact")
                {
                    final Intent intent = new Intent(Intent.ACTION_INSERT_OR_EDIT);
                    intent.putExtra(ContactsContract.Intents.Insert.PHONE, task.getContact_no());
                    intent.putExtra(ContactsContract.Intents.Insert.NAME, task.getContact_name());
                    intent.setType(Contacts.People.CONTENT_ITEM_TYPE);
                    mContext.startActivity(intent);
                }
                else if(menuItem.getTitle()=="Call")
                {
                    taskid=task.getId();
                    taskcreated_at=task.getCreated_at();
                    if(taskcreated_at.equals(""))
                    {
                        SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        TaskAdapter.taskcreated_at=sdff.format(new Date());
                    }
                    else
                    {
                        taskcreated_at=task.getCreated_at();
                    }
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:"+task.getContact_no().toString().trim()));
                    mContext.startActivity(callIntent);
                    Toast.makeText(mContext, "calling "+task.getContact_name(), Toast.LENGTH_SHORT).show();
                }

                return false;


            }
        });

    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

}