package com.jenya.didbizdialer.adapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jenya.didbizdialer.R;
import com.jenya.didbizdialer.model.message;

import java.util.List;

/**
 * Created by Mitesh Makwana on 29/06/18.
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {

    private Context mContext;
    private List<message> messageList;

    Typeface tf;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,number;
        ImageView imginfo;
        RelativeLayout main;
        LinearLayout viewline;

        public MyViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.tvContactName);
//            event = (TextView) view.findViewById(R.id.txteventtitle);
            number = (TextView) view.findViewById(R.id.tvmessage);
            imginfo = (ImageView) view.findViewById(R.id.imginfo);

//            cdview = (CardView) view.findViewById(R.id.card_view);
            main = (RelativeLayout) view.findViewById(R.id.main);
            viewline = (LinearLayout) view.findViewById(R.id.view);

        }

    }

    public MessageAdapter(Context mContext, List<message> messageList) {
        this.mContext = mContext;
        this.messageList = messageList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_message, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
        holder.name.setTypeface(tf);
        holder.number.setTypeface(tf);

        final message message = messageList.get(position);
        Log.e("name",message.getSms_to());

        if(message.getIn_out().equals("1"))
        {
//            holder.imginfo.setColorFilter(Color.argb(255, 255, 255, 255)); // White Tint
            holder.imginfo.setBackgroundResource(R.drawable.ic_call_received_black_24dp);
            String name=getContactName(message.getSms_from().toString(),mContext);
            if(name.equals(""))
            {
                holder.name.setText(message.getSms_from());
            }
            else
            {
                holder.name.setText(name);
            }
        }
        else
        {
            holder.imginfo.setBackgroundResource(R.drawable.ic_call_made_rad_24dp);
            String name=getContactName(message.getSms_from().toString(),mContext);
            if(name.equals(""))
            {
                holder.name.setText(message.getSms_to());
            }
            else
            {
                holder.name.setText(name);
            }

        }

        holder.imginfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "click", Toast.LENGTH_SHORT).show();
//                showmessagedetaildialog(view,message);
            }
        });

        holder.number.setText(message.getMessage());

        if(position==messageList.size()-1)
        {
            holder.viewline.setVisibility(View.GONE);
        }
        else
        {
            holder.viewline.setVisibility(View.VISIBLE);
        }

    }

    public String getContactName(final String phoneNumber, Context context)
    {
        Uri uri=Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};

        String contactName="";
        Cursor cursor=context.getContentResolver().query(uri,projection,null,null,null);

        if (cursor != null) {
            if(cursor.moveToFirst()) {
                contactName=cursor.getString(0);
            }
            cursor.close();
        }

        return contactName;
    }
    @Override
    public int getItemCount() {
        return messageList.size();
    }

}