package com.jenya.didbizdialer.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.jenya.didbizdialer.R;
import com.jenya.didbizdialer.model.calllog;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mitesh Makwana on 19/05/16.
 */
public class CallLogAdapter extends RecyclerView.Adapter<CallLogAdapter.MyViewHolder> {

    private Context mContext;
    private List<calllog> albumList;

    Typeface tf;

    ArrayList<Integer> arryfavperformarid ;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,duration,time;
        ImageView image,callstatus,call;
        CardView cdview;

        public MyViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.tvname);
            duration = (TextView) view.findViewById(R.id.tvduration);
            time = (TextView) view.findViewById(R.id.tvtime);
            image=(ImageView)view.findViewById(R.id.imguser);
            callstatus=(ImageView)view.findViewById(R.id.imgcallstatus);
            call=(ImageView)view.findViewById(R.id.imgcall);
            cdview = (CardView) view.findViewById(R.id.card_view);
        }
    }

    public CallLogAdapter(Context mContext, List<calllog> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_calllog, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        //FetchImages();

        arryfavperformarid = new ArrayList<Integer>();
        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/HKNova-Medium.ttf");
//        tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/proximanovaregular.ttf");
        holder.name.setTypeface(tf);
        holder.duration.setTypeface(tf);
        holder.time.setTypeface(tf);

        final calllog calllog = albumList.get(position);

        String contactname=getContactName(calllog.getNumber(),mContext);
//        Log.e(contactname,"CONTACT");

        if(contactname.equals(""))
        {
            holder.name.setText(calllog.getNumber());
        }
        else
        {
            holder.name.setText(contactname);
        }

        holder.duration.setText(calllog.getDuration());
        holder.time.setText(calllog.getUpdated_at());

        if(calllog.getType().equals("1"))
        {
            holder.callstatus.setBackgroundResource(R.drawable.ic_call_received_black_24dp);

            /*holder.callstatus.setColorFilter(ContextCompat.getColor(mContext, R.color.green), android.graphics.PorterDuff.Mode.MULTIPLY);
            Glide.with(mContext)
                    .load(R.drawable.ic_call_received_black_24dp)
                    .into(holder.callstatus);*/
        }
        else if(calllog.getType().equals("2"))
        {
            holder.callstatus.setBackgroundResource(R.drawable.ic_call_made_black_24dp);

/*            holder.callstatus.setColorFilter(ContextCompat.getColor(mContext, R.color.green), android.graphics.PorterDuff.Mode.MULTIPLY);

            Glide.with(mContext)
                    .load(R.drawable.ic_call_made_black_24dp)
                    .into(holder.callstatus);*/
        }
        else if(calllog.getType().equals("3"))
        {

            holder.callstatus.setBackgroundResource(R.drawable.ic_call_missed_outgoing_black_24dp);

            /*holder.callstatus.setColorFilter(ContextCompat.getColor(mContext, R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);

            Glide.with(mContext)
                    .load(R.drawable.ic_call_missed_outgoing_black_24dp)
                    .into(holder.callstatus);*/
        }


        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:"+calllog.getNumber()));
                mContext.startActivity(callIntent);

            }
        });

        /*Glide.with(mContext)
                .load(Config.PERFORMERSIMAGE+album.getPerformer_image())
                .into(holder.image);*/

        /*holder.cdview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });*/

        holder.cdview.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                showPopupstatus(view,calllog);
                return false;
            }
        });

    }

    public String getContactName(final String phoneNumber, Context context)
    {
        Uri uri=Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};

        String contactName="";
        Cursor cursor=context.getContentResolver().query(uri,projection,null,null,null);

        if (cursor != null) {
            if(cursor.moveToFirst()) {
                contactName=cursor.getString(0);
            }
            cursor.close();
        }

        return contactName;
    }

    public void showPopupstatus(View v, final calllog calllog) {

        PopupMenu popup = new PopupMenu(mContext, v);
        int p=1;
        popup.getMenu().add("Message");

        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if(menuItem.getTitle()=="Message") {
                    Intent intentsms = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"+ calllog.getNumber()));
                    intentsms.putExtra("sms_body", "");
                    mContext.startActivity(intentsms);
                }
                return false;
            }
        });

    }


    @Override
    public int getItemCount() {
        return albumList.size();
    }

}