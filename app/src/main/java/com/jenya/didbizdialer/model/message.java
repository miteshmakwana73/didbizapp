package com.jenya.didbizdialer.model;

public class message {
    private int id;
    private String sms_from;
    private String sms_to;
    private String user_id;
    private String message;
    private String type;
    private String in_out;
    private String created_at;
    private String updated_at;

    public message(int id, String sms_from, String sms_to, String user_id, String message, String type, String in_out, String created_at, String updated_at) {
        this.id = id;
        this.sms_from = sms_from;
        this.sms_to = sms_to;
        this.user_id = user_id;
        this.message = message;
        this.type = type;
        this.in_out = in_out;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSms_from() {
        return sms_from;
    }

    public void setSms_from(String sms_from) {
        this.sms_from = sms_from;
    }

    public String getSms_to() {
        return sms_to;
    }

    public void setSms_to(String sms_to) {
        this.sms_to = sms_to;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIn_out() {
        return in_out;
    }

    public void setIn_out(String in_out) {
        this.in_out = in_out;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}