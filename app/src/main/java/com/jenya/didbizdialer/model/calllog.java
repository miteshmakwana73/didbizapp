package com.jenya.didbizdialer.model;

public class calllog {
    private int ID;
    private String number;
    private String type;
    private String duration;
    private String task_id;
    private String user_id;
    private String recording;
    private String created_at;
    private String updated_at;

    public calllog(int ID, String number, String type, String duration, String task_id, String user_id, String recording, String created_at, String updated_at) {
        this.ID = ID;
        this.number = number;
        this.type = type;
        this.duration = duration;
        this.task_id = task_id;
        this.user_id = user_id;
        this.recording = recording;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRecording() {
        return recording;
    }

    public void setRecording(String recording) {
        this.recording = recording;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
