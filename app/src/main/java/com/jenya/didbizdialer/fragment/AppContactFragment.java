package com.jenya.didbizdialer.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.jenya.didbizdialer.R;
import com.jenya.didbizdialer.adapter.AllContactsAdapter;
import com.jenya.didbizdialer.jsonurl.Config;
import com.jenya.didbizdialer.model.ContactVO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AppContactFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<ContactVO> contactList;
    private List<String> didbizList;
    ProgressBar progressBar;
    SwipeRefreshLayout sw_refresh;

    TextView status;

    String HttpUrl = Config.URL_DIDBIZ_CONTACT;
    String user_id,username,email,primary_contact,department_id,department_name,name,api_key,password;

    public AppContactFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       /* // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_appcontact, container, false);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("MyFile", 0);
        user_id = sharedPreferences.getString("id","");
        username = sharedPreferences.getString("username","");
        email = sharedPreferences.getString("email","");
        primary_contact = sharedPreferences.getString("primary_contact","");
        department_id = sharedPreferences.getString("department_id","");
        department_name = sharedPreferences.getString("department_name","");
//        name = sharedPreferences.getString("name","");
        f_name = sharedPreferences.getString("first_name","");
        l_name = sharedPreferences.getString("last_name","");
        api_key = sharedPreferences.getString("api_key","");
        password = sharedPreferences.getString("password","");

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        status=(TextView)view.findViewById(R.id.tvstatus);
        sw_refresh = (SwipeRefreshLayout) view.findViewById(R.id.sw_refresh);

        sw_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        // do something...
                        contactList.clear();

                        checkconnection();

                        sw_refresh.setRefreshing(false);

                    }
                }, 1000);

            }
        });
        contactList = new ArrayList<>();
        didbizList = new ArrayList<>();

//        adapter = new CallLogAdapter(getActivity(),contactList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(adapter);

        checkconnection();*/
        return null;
    }

    private void checkconnection() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        android.net.NetworkInfo wifi = cm
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        android.net.NetworkInfo datac = cm
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if ((wifi != null & datac != null)
                && (wifi.isConnected() | datac.isConnected())) {
            //connection is avlilable

            status.setVisibility(View.GONE);

            getcontactlist();
//            getdidbizContacts();

        }else {
            //no connection
            status.setVisibility(View.VISIBLE);

            Toast.makeText(getActivity(), "No Connection Found", Toast.LENGTH_SHORT).show();
        }
    }

    private void getcontactlist() {
        progressBar.setVisibility(View.VISIBLE);

        didbizList.clear();
        // Creating string request with post method.
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status1 = jobj.getString("success");

//                            String msg = jobj.getString("message");

                            if (status1.equals("true")) {

                                //move to next page
//                                Toast.makeText(PerformarEventActivity.this, msg, Toast.LENGTH_SHORT).show();

//                                loadPerformarevent();


                                JSONArray heroArray = jobj.getJSONArray("user_data");

                                //now looping through all the elements of the json array
                                for (int i = 0; i < heroArray.length(); i++) {
                                    //getting the json object of the particular index inside the array
                                    JSONObject heroObject = heroArray.getJSONObject(i);

                                    //creating a hero object and giving them the values from json object

                                    /*task hero = new task(
                                            heroObject.getInt("user_id"),
                                            heroObject.getString("username"),
                                            heroObject.getString("email"),
                                            heroObject.getString("display_name"),
                                            heroObject.getString("primary_contact"));*/

                                    //adding the hero to herolist
                                    didbizList.add(heroObject.getString("primary_contact"));
                                }

                                getdidbizContacts();
                                //Log.e("list", String.valueOf(albumList));


                            } else {
//                                status.setText(msg);
                                status.setVisibility(View.VISIBLE);

//                                showToast(msg);

                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
//                            Log.e("success",msg);
                        }catch (Exception e) {
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));
                            getsingletask();

                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                /*String credentials = "username:password";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);*/
                headers.put("x-api-key", api_key);

                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                params.put("user_id", String.valueOf(1));
                params.put("department_id", String.valueOf(1));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void getsingletask() {
        progressBar.setVisibility(View.VISIBLE);

        // Creating string request with post method.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HttpUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);
                        try {

                            JSONObject jobj = new JSONObject(ServerResponse);

                            String status = jobj.getString("success");

//                            String msg = jobj.getString("message");

                            if (status.equals("true")) {

                                //move to next page
//                                Toast.makeText(PerformarEventActivity.this, msg, Toast.LENGTH_SHORT).show();

//                                loadPerformarevent();


                                JSONObject heroArray = jobj.getJSONObject("user_data");

                                /*task hero = new task(
                                        heroArray.getInt("user_id"),
                                        heroArray.getString("username"),
                                        heroArray.getString("email"),
                                        heroArray.getString("display_name"),
                                        heroArray.getString("primary_contact"));*/


                                didbizList.add(heroArray.getString("primary_contact"));

                                getdidbizContacts();


                            } else {
//                                showToast(msg);
                            }

                            //Toast.makeText(SignupActivity.this, ServerResponse, Toast.LENGTH_LONG).show();
//                            Log.e("success",msg);
                        }catch (Exception e) {
                            status.setText("NO DATA");
                            status.setVisibility(View.VISIBLE);
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e));
                        }

                        // Showing response message coming from server.

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        // Hiding the progress dialog after all task complete.
                        progressBar.setVisibility(View.GONE);

                        if( volleyError instanceof NoConnectionError) {
                            showToast("No connection available");
                        }
                        else if (volleyError.getClass().equals(TimeoutError.class)) {
                            // Show timeoutbuttonSignup error message
                            showToast("Oops. Timeout error!");
                        }
                        else if (volleyError instanceof ServerError) {
                            // Show timeout error message
                            showToast("Server error!");
                        }
                        else
                        {
                            showToast(volleyError.toString());
                        }

                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                /*String credentials = "username:password";
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", auth);*/
                headers.put("x-api-key", api_key);//1234
                /*headers.put("user_id", String.valueOf(1));
                headers.put("department_id", String.valueOf(1));*/

                return headers;
            }

            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();


                // Adding All values to Params.
                params.put("user_id", String.valueOf(user_id));
                params.put("department_id", String.valueOf(department_id));

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);
    }

    private void getdidbizContacts() {
        List<ContactVO> contactVOList = new ArrayList();
        ContactVO contactVO;

            for (int i = 0; i < didbizList.size(); i++) {
//                            Log.e("comparing",didbizList.get(i));
                String didbizcontact=didbizList.get(i);


                String comparenumber = getContactName(didbizcontact,getActivity());

                if(comparenumber.equals(""))
                {

                }
                else
                {
                    contactVO = new ContactVO();
                    contactVO.setContactName(comparenumber);//name
                    contactVO.setContactNumber(didbizcontact);
                    contactVOList.add(contactVO);                }

                           /* if(didbizcontact.contains(comparenumber))
                            {
                                contactVO.setContactNumber(phoneNumber);//phoneNumber
                                Log.e("compare match",phoneNumber);

                                contactVOList.add(contactVO);


                            }*/
            }

            AllContactsAdapter contactAdapter = new AllContactsAdapter(contactVOList, getActivity());
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(contactAdapter);

        if(contactVOList.size()==0)
        {
            status.setText("No Didbiz Contacts");
            status.setVisibility(View.VISIBLE);
        }
    }

    public String getContactName(final String phoneNumber, Context context)
    {
        Uri uri=Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};

        String contactName="";
        Cursor cursor=context.getContentResolver().query(uri,projection,null,null,null);

        if (cursor != null) {
            if(cursor.moveToFirst()) {
                contactName=cursor.getString(0);
            }
            cursor.close();
        }

        return contactName;
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    public int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public void showToast(String msg)
    {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }


}
